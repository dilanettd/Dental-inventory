exports.up = function (knex) {
    return knex.schema
        .createTable("patient", function (table) {
            table.string("matricule", 255).primary();
            table.string("nom", 255).notNullable();
            table.string("prenom", 255).notNullable();
            table.string("dateN", 255).notNullable();
            table.string("lieuN", 255).notNullable();
            table.string("telephone", 255).notNullable();
            table.string("mail", 255).notNullable();
            table.string("sexe", 255).notNullable();
            table.string("adresse", 255).notNullable();
            table.string("typeClient", 255).notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("produit", function (table) {
            table.increments("id").primary();
            table.string("nom", 255).notNullable();
            table.string("nomCommercial", 255).notNullable();
            table.string("image", 255).notNullable();
            table.integer("quantite").notNullable();
            table.string("datePeremption", 255).notNullable();
            table.string("conditionnement", 255).notNullable();
            table.string("fabricant", 255).notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("soins", function (table) {
            table.increments("id").primary();
            table.string("nom", 255).notNullable();
            table.string("description", 255).notNullable();
            table.integer("prixContoire").notNullable();
            table.integer("prixAssurer").notNullable();
            table.integer("prixPersonnel").notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("utilisateur", function (table) {
            table.increments("id").primary();
            table.string("login", 255).notNullable();
            table.string("password", 255).notNullable();
            table.string("nom", 255).notNullable();
            table.string("typeUser", 255).notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("consultation", function (table) {
            table.increments("id").primary();
            table.string("matricule", 255).references("matricule").inTable("patient");
            table.integer("commentaire", 1000).notNullable();;
            table.string("jour", 255).notNullable();
            table.string("moi", 255).notNullable();
            table.string("annee", 255).notNullable();
            table.integer("prix").notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("consultationSoins", function (table) {
            table.increments("id").primary();
            table.string("idConsultatltion", 255).references("id").inTable("consultation");
            table.integer("idSoins").references("id").inTable("soins");
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        })
        .createTable("action", function (table) {
            table.increments("id").primary();
            table.integer("idProduit").references("id").inTable("produit");
            table.string("type", 255).notNullable();
            table.integer("quantite").notNullable();
            table.string("jour", 255).notNullable();
            table.string("moi", 255).notNullable();
            table.string("annee", 255).notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("updated_at").defaultTo(knex.fn.now());
        });
};


exports.down = function (knex) {
    return knex.schema
        .dropTable("patient")
        .dropTable("produit")
        .dropTable("soins")
        .dropTable("utilisateur")
        .dropTable("consultation")
        .dropTable("action");

};
