export enum TypeAction {
    ADD_ACTION="add_action",
    UPDATE_ACTION="update_action",
    DELETE_ACTION="delete_action"
}

export enum TypePatient {
    SIMPLE="simple",
    ASSURER="assurer",
    PERSONNEL="personnel"
}
export enum TypeUser {
    SIMPLE = "simple_user",
    ADMIN ="admin_user"
}

