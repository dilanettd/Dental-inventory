import { TypePatient } from "../enums";
import { Entity } from "./Entity";



export class Patient extends Entity {
    matricule: string;
    nom: string;
    prenom: string;
    dateN: string;
    lieuN: string;
    tel: string;
    mail: string;
    adresse: string;
    typePatient: string;
    sexe: string;

    constructor(
            matricule="",
            nom="",
            prenom="",
            dateN="",
            lieuN="",
            tel="",
            mail="",
            adresse="",
            typePatient=TypePatient.PERSONNEL,
            sexe=""
    ) {
        super("");
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.dateN = dateN;
        this.lieuN = lieuN;
        this.tel = tel;
        this.mail = mail;
        this.adresse = adresse;
        this.typePatient = typePatient;
        this.sexe=sexe;
    }
    static generatedMatricule():string
    {
        let date = new Date()
        return `CD${date.getFullYear()}${date.getMonth() + 1}P${date.getDate()}${Math.floor(Math.random() * 1000)}`;
    }
    toJSON(): Record<string, any> {
        return {
            // ...super.toJSON(),
            matricule:this.matricule,
            nom:this.nom,
            prenom:this.prenom,
            dateN:this.dateN,
            lieuN:this.lieuN,
            telephone:this.tel,
            mail:this.mail,
            sexe:this.sexe,
            typeClient:this.typePatient,
            adresse:this.adresse
        }
    }
}

