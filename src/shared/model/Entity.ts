export class Entity {
    id:String=""
    constructor(id="")
    {
        this.id=id;
    }
    static generatedID()
    {
        return `${Math.floor(Math.random()*100)}`
    }

    toJSON():Record<string,any>
    {
        return {
            "id":this.id
        }
    }
}
