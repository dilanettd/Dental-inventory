import { Entity } from "./Entity";


export class Produit  extends Entity{
    nom: string;
    nomCommercial: string;
    imageUrl: string;
    quantite: number;
    datePeremption: string;
    fabricant: string;
    conditionnement: string;

    constructor(
            id="",
            nom="",
            nomCommercial="",
            imageUrl="",
            quantite=0,
            conditionnement="",
            datePeremption="",
            fabricant=""
    ) {
        super(id);
        this.nom = nom;
        this.nomCommercial = nomCommercial;
        this.imageUrl = imageUrl;
        this.quantite = quantite;
        this.datePeremption = datePeremption;
        this.fabricant=fabricant;
        this.conditionnement=conditionnement;

    }

    toJSON(): Record<string, any> {
        return {
            ...super.toJSON(),
            nom:this.nom,
            nomCommercial:this.nomCommercial,
            image:this.imageUrl,
            quantite:this.quantite,
            datePeremption:this.datePeremption,
            conditionnement:this.conditionnement,
            fabricant:this.fabricant
        }
    }
}
