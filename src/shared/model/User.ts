import { TypeUser } from "../enums";
import { Entity } from "./Entity";

export class User extends Entity
{
    public login=""
    public password=""
    public typeUser:TypeUser=TypeUser.SIMPLE
    public nom=""
    constructor(id="",login="",password="",nom="",typeUser=TypeUser.SIMPLE) {
        super(id);
        this.login = login;
        this.password=password;
        this.nom=nom;
        this.typeUser = typeUser;
    }
    toJSON(): Record<string, any> {
        return {
            ...super.toJSON(),
            login:this.login,
            password:this.password,
            nom:this.nom,
            typeUser:this.typeUser
        }
    }
}
