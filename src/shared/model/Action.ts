import { TypeAction } from "../enums";
import { PeriodicEntity } from "./PeriodicEntity";
import { Produit } from "./Produit";


export class Action extends PeriodicEntity {
    
    typeAction: string;
    quantite: number;
    produit: Produit;

    constructor(
        id,
        typeAction=TypeAction.ADD_ACTION,
        quantite=0,
        jour="",
        mois="",
        annee="",
        produit=new Produit()
        ) {
        super(id,annee,mois,jour);
        this.typeAction=typeAction;
        this.quantite=quantite;
        this.produit=produit;
    }  
    getTypeAction() {
        return this.typeAction
    }
    toJSON(): Record<string, any> {
        return {
            ...super.toJSON(),
            type:this.typeAction,
            quantite:this.quantite,
            idProduit:this.produit.id
        }
    }
}

