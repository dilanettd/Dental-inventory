import { Patient } from "./Patient";
import { PeriodicEntity } from "./PeriodicEntity";
import { Soins } from "./Soins";

export class Consultation extends PeriodicEntity {
    
    prix: number;
    patient: Patient;
    soins: Soins[]=[];
    commentaire:string;
    constructor(
        id="",
        prix=0,
        jour="",
        mois="",
        annee="",
        patient = new Patient(),
        soins=[],
        commentaire=""
        ) {
        super(id,annee,mois,jour);
        this.prix = prix;
        this.patient=patient;
        this.soins=soins;
        this.commentaire=commentaire
    }
    getPrix(): number {
        return this.prix
    }

    toJSON(): Record<string, any> {
        return {
           ...super.toJSON(),
            matricule:this.patient.matricule,
            prix:this.prix,
            commentaire:this.commentaire
        }
    }



}

