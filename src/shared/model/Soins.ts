import { Entity } from "./Entity";

export class Soins extends Entity{
    nom: string;
    description: string;
    prixContoire: number;
    prixAssurer: number;
    prixPersonnel: number;

    constructor(
            id="",
            nom="",
            description="",
            prixContoire=0,
            prixAssurer=0,
            prixPersonnel=0
    ) {
        super(id);
        this.nom = nom;
        this.description = description;
        this.prixContoire = prixContoire;
        this.prixAssurer = prixAssurer;
        this.prixPersonnel = prixPersonnel;
    }
    toJSON(): Record<string, any> {
        return {
            ...super.toJSON(),
            nom:this.nom,
            description:this.description,
            prixContoire:this.prixContoire,
            prixAssurer:this.prixAssurer,
            prixPersonnel:this.prixPersonnel
        }
    }
}

