import { Entity } from "./Entity";

export class PeriodicEntity extends Entity {
    annee: string;
    moi: string;
    jour: string;

    constructor(id="",annee="",mois="",jour="")
    {
        super(id);
        this.annee=annee;
        this.moi=mois;
        this.jour=jour;
    }
    getAnnee()
    {
        return this.annee
    }
    getMois()
    {
        return this.moi
    }
    getJour()
    {
        return this.jour
    }
    toJSON(): Record<string, any>
    {
        return {
            ...super.toJSON(),
            jour:this.jour,
            moi:this.moi,
            annee:this.annee            
        }
    }
}

