import { Action } from "./Action";
import { Consultation } from "./Consultation";
import { Entity } from "./Entity";
import { Patient } from "./Patient";
import { PeriodicEntity } from "./PeriodicEntity";
import { Produit } from "./Produit";
import { Soins } from "./Soins";
import { User } from "./User";

export {
    Entity,
    Action,
    Consultation,
    Patient,
    PeriodicEntity,
    Produit,
    Soins,
    User
}