import { configDB } from "./knexfile";

const knex = require('knex')

export class DataBaseAccess {
    static dbInstance = null

    static getInstance() {
        if(DataBaseAccess.dbInstance==null)DataBaseAccess.dbInstance=knex(configDB.development);
        return DataBaseAccess.dbInstance
    }
}