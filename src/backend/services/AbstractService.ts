import { Entity } from "../../shared/model";
import { DataBaseAccess } from "../databaseaccess/DataBaseAcceess";
import { ActionResult } from "../utils/ActionResult";

export abstract class AbstractService<T extends Entity> {
    list:T[]=[]
    tablename:string=""
    daoController=null;
    db:any=null

    constructor()
    {
        this.db=DataBaseAccess.getInstance()
    }
    
    countAllObject():Number
    {
        return this.list.length;
    }
    countAllByField(field,value):Number
    {
        return this.findByField(field,value).length;
    }
   

    findByField(field,value):T[]  {
        return this.list.filter((f:any)=> Reflect.get(f,field)==value);
    }

    getList():T[] {
        return this.list;
    }

    getDaoController() {
        return this.daoController;
    }
    abstract saveLoadedData(dataList): Promise<ActionResult<boolean>>
   
    loadData(): Promise<ActionResult<boolean>>
    {
        let action = new ActionResult<boolean>()
        return new Promise<ActionResult<boolean>>((resolve,reject)=>{
            this.db(this.tablename)
            .select([])
            .then((result)=>this.saveLoadedData(result))
            .then((result)=>{
                action.result = true;
                resolve(action)
            })
            .catch((error)=>{
                action.result=error
                console.log("Error",error)
                reject(action)
            })
        })
        // ResultRequest<ArrayList<T>> result= daoController.fetchAll();
        // if (result.getStatut()!=ResultRequest.OK) return null;
        // this.list.pushAll(result.getResult());
        // result.setResult(null);
        // return result;
    }
    save(data:T):Promise<ActionResult<boolean>>
    {
       
        return new Promise<ActionResult<boolean>>((resolve,reject)=>{
            this.db(this.tablename).insert(data.toJSON())
            .then((resultID)=>{
                data.id=resultID
                this.list.push(data);
                let r = new ActionResult<boolean>()
                r.result = true;
                resolve(r)
            })
            .catch((error)=>{
                console.log("Error ",error)
                let r = new ActionResult<boolean>()
                r.result = false;
                reject(r)
            })
            
        })

    }
    update(data:T)
    {
        return new Promise<ActionResult<boolean>>((resolve,reject)=>{
            this.db(this.tablename)
            .where((builder)=>builder.where({"id":data.id}))
            .update(data.toJSON())
            .then((r)=>{
                let pos = this.list.findIndex((value) => value.id == data.id);
                if (pos >= 0) this.list[pos] = data;
                let result = new ActionResult<boolean>();
                result.result = true;
                resolve(result)
            })
            .catch((error)=>{
                console.log("Error ", error)
                let r = new ActionResult<boolean>()
                r.result = false;
                reject(r)
            })
            
        })
    }
    delete(data:T):Promise<ActionResult<boolean>>
    {
        return new Promise<ActionResult<boolean>>((resolve, reject) => {
            this.db(this.tablename)
                .where((builder) => builder.where({ "id": data.id }))
                .delete()
                .then((r) => {
                    let pos = this.list.findIndex((value) => value.id == data.id);
                    if (pos >= 0) this.list.splice(pos, 1);

                    let result = new ActionResult<boolean>();
                    result.result = true;
                    resolve(result)
                })
                .catch((error) => {
                    console.log("Error ", error)
                    let r = new ActionResult<boolean>()
                    r.result = false;
                    reject(r)
                })

        })
       
    }

}
