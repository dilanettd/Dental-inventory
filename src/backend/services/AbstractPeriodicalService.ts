import { Entity } from "../../shared/model";
import { AbstractService } from "./AbstractService";

export abstract class AbstractPeriodicalService<T extends Entity> extends AbstractService<T> {
    
    getListObjectsByJour(year:String, mois:String, jour:String):T[]  {
        let listData = [];
        return this.list.filter((e:any)=>  e.getJour()==jour && e.getAnnee()==year && e.getMois()==mois);
    }

    
    getListObjectsByMois(annee:String,mois:String):T[]  {
        let listData = [];
        this.list.filter((action:any)=> action.getMois()==mois && action.getAnnee()==annee).forEach((action => listData.push(action)));
        return listData;
    }

    
    getListObjectsByAnnee(annee:String):T[] {
        let listData = [];
        this.list.filter((action:any)=> action.getAnnee()==annee).forEach((action => listData.push(action)));
        return listData;
    }

    
   getNumberObjectByYear(annee:String):Number {
        return this.getListObjectsByAnnee(annee).length;
    }

    
    getNumberObjectByMonth(annee:String, month:String):Number {
        return this.getListObjectsByMois(annee,month).length;
    }

    
    getNumberObjectByDay(annee:String, month:String, day:String):Number {
        return this.getListObjectsByJour(annee, month, day).length;
    }
}

