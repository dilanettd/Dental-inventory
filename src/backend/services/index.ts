import { AuthService } from "./AuthService";
import { ConsultationService } from "./ConsultationService";
import { HistoriqueService } from "./HistoriqueService";
import { PatientService } from "./PatientService";
import { SoinsService } from "./SoinsService";
import { StartService } from "./StartService";
import { StockService } from "./StockService";



export{
    ConsultationService,
    StockService,
    AuthService,
    HistoriqueService,
    PatientService,
    SoinsService,
    StartService
}