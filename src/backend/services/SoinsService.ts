import { Soins } from "../../shared/model";
import { ActionResult } from "../utils/ActionResult";
import { AbstractService } from "./AbstractService";

export class SoinsService extends AbstractService<Soins>{
    constructor()
    {
        super()
        this.tablename="soins"
        this.daoController=null;
    }
    saveLoadedData(dataList): Promise<ActionResult<boolean>> {
        return new Promise<ActionResult<boolean>>((resolve, reject) => {
            this.list = dataList.map((data) => new Soins(data.id, data.nom, data.description, data.prixContoire, data.prixAssurer, data.prixPersonnel))
            let action = new ActionResult<boolean>(0, "reussi", '', true)
            resolve(action)
        })
    }
}
