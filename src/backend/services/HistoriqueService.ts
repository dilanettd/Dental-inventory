import { StockService } from ".";
import { TypeAction } from "../../shared/enums";
import { Action } from "../../shared/model";
import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";
import { AbstractPeriodicalService } from "./AbstractPeriodicalService";


export class HistoriqueService extends AbstractPeriodicalService<Action> {

    constructor()
    {
        super()
        this.daoController=null;
 
        this.tablename="action"
    }
    saveLoadedData(dataList): Promise<ActionResult<boolean>> {
        return new Promise < ActionResult < boolean >>((resolve,reject)=>{
            console.log(dataList)
            this.list.push(...dataList.map((data) => new Action(data.id,data.type,data.quantite,data.jour,data.moi,data.annee,
                InjectorContainer.getInstance().getInstanceOf<StockService>(StockService).findByField("id",data.idProduit)[0],)))
            let action = new ActionResult<boolean>(0, "reussi", '', true)
            resolve(action)
        })
    }
    getNumberProductpushedByDay(year,month,day)
    {
        return this.getListObjectsByJour(year, month, day)
                .filter(action=>action.getTypeAction()== TypeAction.DELETE_ACTION)
                .length;
    }

    getActionListByTypeAction(actions,actionType):Action[]
    {
        let listData = [];
        actions.filter(action => action.getTypeAction()==actionType).forEach((e => listData.push(e)));
        return listData;
    }

    getActionListByTypeByDay(annee,month,day,type):Action[]
    {
        let listData = [];
        return this.getActionListByTypeAction(this.getListObjectsByJour(annee, month, day),type);
    }

    getActionListByTypeByMonth(annee,month,type):Action[]
    {
        let listData = [];
        return this.getActionListByTypeAction(this.getListObjectsByMois(annee, month),type);
    }
    getActionListByTypeByYear(annee,type):Action[]
    {
        let listData = [];
        return this.getActionListByTypeAction(this.getListObjectsByAnnee(annee),type);
    }

    getQuantityByTypeByMonth(year,month,type)
    {
        return this.getQuantity(this.getActionListByTypeByMonth(year,month,type));
    }

    getQuantityByTypeByYear(year,type)
    {
        return this.getQuantity(this.getActionListByTypeByYear(year,type));
    }

    getQuantityByTypeByDay(year,month,day,type)
    {
        return this.getQuantity(this.getActionListByTypeByDay(year,month,day,type));
    }

    getQuantity(list)
    {
        return list
                .map(action=>action.getQuantite())
                .reduce(0,(qteTotal,qte)=>qteTotal+qte);
    }
}


