import { PatientService } from "./PatientService";
import { StartService } from "./StartService"
import { TypeUser } from "../../shared/enums";
import { User } from "../../shared/model/User";
import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";

export class AuthService {

    // private DAOUserController daoController= new DAOUserController();
    private currentUser:User
    constructor()
    {
        this.currentUser=new User("jklqsdfjq","admin","admin","Admin",TypeUser.ADMIN);
    }

    getCurrentUser() {
        return this.currentUser;
    }

    getDaoController() {
        // return daoController;
    }

    login(login:String, password:String):Promise<ActionResult<boolean>>
    {
        // this.currentUser=daoController.connexion(login,password);
        return new Promise<ActionResult<boolean>>((resolve,reject)=>{
            let result:ActionResult<boolean> = new ActionResult<boolean>();
            if(login==this.currentUser.login && password==this.currentUser.password) {
                result.result = true;
                InjectorContainer.getInstance().getInstanceOf<StartService>(StartService)
                .loadData()
                .then((result)=>resolve(result))
                .catch((error)=>reject(error))
                // InjectorContainer.getInstance().getInstanceOf<StartService>(StartService).loadData()
            }
            else result.result=false;
            resolve(result)
        })
    }

    logout()
    {
        this.currentUser=null;
    }
}
