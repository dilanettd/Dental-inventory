import { HistoriqueService } from "./HistoriqueService"
import { TypeAction } from "../../shared/enums"
import { Action, Entity, Produit } from "../../shared/model"
import { ActionResult } from "../utils/ActionResult"
import { InjectorContainer } from "../utils/lifecycle/injector_container"
import { AbstractService } from "./AbstractService"

export class StockService extends AbstractService<Produit> {

    protected historiqueService:HistoriqueService=null;
    constructor()
    {
        super()
        // daoController=new DAOProduitController();
        this.tablename="produit"
    }
    changeQuantiti(data:Produit,quantite=0,act)
    {
        let action=new Action(Entity.generatedID(),act,quantite,`${(new Date()).getDate()}`,`${(new Date()).getMonth()+1}`,`${(new Date()).getFullYear()}`)
        action.produit=data;
        if(act==TypeAction.ADD_ACTION) data.quantite+=quantite
        else if(act==TypeAction.DELETE_ACTION) data.quantite-=quantite
        // console.log("historiqueService: ", InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService))
        return InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService).save(action).then(()=>super.update(data))   
    }
    saveLoadedData(dataList): Promise<ActionResult<boolean>> {
        return new Promise<ActionResult<boolean>>((resolve, reject) => {
            this.list = dataList.map((data) => new Produit(data.id, data.nom, data.nomCommercial, data.image, data.quantite, data.conditionnement, data.datePeremption, data.fabricant))
            let action = new ActionResult<boolean>(0, "reussi", '', true)
            resolve(action)
        })
    }
}
