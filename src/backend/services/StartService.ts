// const { AuthService } = require("./AuthService");
// const { InjectorContainer } = require("../utils/lifecycle/injector_container.ts");
// const { ConsultationService } = require("./ConsultationService");
// const { HistoriqueService } = require("./HistoriqueService");
// const { PatientService } = require("./PatientService");
// const { StockService } = require("./StockService");
// const { SoinsService } = require("./SoinsService");

import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";
import { AuthService } from "./AuthService";
import { ConsultationService } from "./ConsultationService";
import { HistoriqueService } from "./HistoriqueService";
import { PatientService } from "./PatientService";
import { SoinsService } from "./SoinsService";
import { StockService } from "./StockService";



export class StartService {
    protected request:any=null;
    protected injectorInstance:InjectorContainer=InjectorContainer.getInstance()
   
    init()
    {
        console.log("init")
        this.injectorInstance.bootstrap([
            {module:AuthService,instance:new AuthService()},
            {module:ConsultationService,instance:new ConsultationService()},
            {module:HistoriqueService,instance:new HistoriqueService()},
            {module:PatientService,instance:new PatientService()},
            {module:SoinsService,instance:new SoinsService()},
            {module:StockService,instance:new StockService()}
        ])

        // requete=DataAccess.connexion(Constante.DB_HOST,Constante.DB_PORT,Constante.DB_NAME,Constante.DB_USERNAME,Constante.DB_PASSWORD);
        // if(requete!=null)
        // {
        //     injectRequete();
        // }
    }
    injectRequete()
    {
        // IOContainer.getAuthServiceInstance().getDaoController().setRequete(requete);
        // IOContainer.getConsultationServiceInstance().getDaoController().setRequete(requete);
        // IOContainer.getHistoriqueServiceInstance().getDaoController().setRequete(requete);
        // IOContainer.getPatientServiceInstance().getDaoController().setRequete(requete);
        // IOContainer.getSoinsServiceInstance().getDaoController().setRequete(requete);
        // IOContainer.getStockServiceInstance().getDaoController().setRequete(requete);
    }
    loadData()
    {
        console.log("Loading data")
        let action = new ActionResult < boolean >();
        return new Promise<ActionResult<boolean>>((resolve,reject)=>{
            InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService).loadData()
            .then((result) => InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService).loadData())
            .then((result) => InjectorContainer.getInstance().getInstanceOf<StockService>(StockService).loadData())
            .then((result) => InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService).loadData())
            .then((result) => InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService).loadData())
            .then((result) => {
                console.log("ok")
                action.result=true;
                resolve(action)
            })
            .catch((error) => {
                console.log("Error", error)
                action.result = false;
                reject(action)
            })
        })   
        
    }
    end()
    {
        // DataAccess.closeConnexion(requete);
    }
}
