import { PatientService, SoinsService } from ".";
import { Consultation, Patient, Soins } from "../../shared/model";
import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";
import { AbstractPeriodicalService } from "./AbstractPeriodicalService";



 export class ConsultationService extends AbstractPeriodicalService<Consultation> {

    constructor()
    {
        // daoController=new DAOConsultationController();
        super()
        this.tablename="consultation"
    }

    getDayPrice(year,month,day)
    {
        return this.getListObjectsByJour(year, month, day)
                .map(consultation =>consultation.getPrix())
                .reduce((amountTotal,consultationAmount)=>amountTotal+consultationAmount,0);
    }
    save(data: Consultation): Promise<ActionResult<boolean>> {

        return new Promise<ActionResult<boolean>>((resolve, reject) => {
                super.save(data)
                .then((result:ActionResult<boolean>)=>{
                    return Promise.all(data.soins.map((soins) => this.db("consultationSoins").insert({
                        "idConsultatltion": data.id,
                        "idSoins":soins.id
                    })))
                   
                })
                .then((result:ActionResult<boolean>[])=>{
                    let r = new ActionResult<boolean>()
                    r.result = true;
                    resolve(r)
                })
                .catch((error) => {
                    console.log("Error ", error)
                    let r = new ActionResult<boolean>()
                    r.result = false;
                    reject(r)
                })

        })

    }
     saveLoadedData(dataList): Promise<ActionResult<boolean>> {
         return new  Promise < ActionResult < boolean >>((resolve,reject)=>{
             console.log("Consultation ", dataList)
             let data=new Map<number,Consultation>()
             let dataid=[]
             dataList.forEach((consultation) => {
                 console.log("consultation",consultation)
                 data.set(parseInt(consultation.id), new Consultation(consultation.id, consultation.prix, consultation.jour, consultation.moi, consultation.annee,
                     InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService).findByField("matricule", consultation.matricule)[0],
                     [], consultation.commentaire
                 ))
                 
                 dataid.push(consultation.id)
             })

             Promise.all(dataid.map((id)=>{
                 return new Promise<Record<string,any>[]>((resolve,reject)=>{
                    this.db("consultationSoins")
                    .select("*")
                    .where("idConsultatltion",'=',id)
                    .then((result)=>{
                        if (result) return resolve(result)
                        return resolve([])
                    })
                    .catch((error)=>reject(error))
                 })
             }))
             .then((result:any[])=>{
                 result.forEach((action)=>{
                     action.forEach(e => {
                         console.log("e" ,e)
                         data.get(parseInt(e.idConsultatltion)).soins.push(InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService).findByField("id",e.idSoins)[0])
                     });
                 })
                 this.list=Array.from(data.values())
                 let r = new ActionResult<boolean>()
                 r.result=true;
                 resolve(r)
             })
             .catch((error)=>{
                 reject(error)
             })
         })       
    }
}
