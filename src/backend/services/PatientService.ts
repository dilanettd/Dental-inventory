import { Patient } from "../../shared/model";
import { ActionResult } from "../utils/ActionResult";
import { AbstractService } from "./AbstractService";


export class PatientService extends AbstractService<Patient> {

    constructor()
    {
        super()
        this.daoController=null;
        this.tablename="patient"
    }

    saveLoadedData(dataList): Promise<ActionResult<boolean>> {
        return new Promise < ActionResult < boolean >>((resolve,reject)=>{
            this.list.push(...dataList.map((data) => new Patient(data.matricule, data.nom, data.prenom, data.dateN, data.lieuN, data.telephone, data.mail, data.adresse, data.typePatient)))
            let action = new ActionResult<boolean>(0,"reussi",'',true)
            resolve(action)
        })
    }
    update(data:Patient)
    {
        return new Promise<ActionResult<boolean>>((resolve, reject) => {
            this.db(this.tablename)
                .where((builder) => builder.where({ "matricule": data.matricule }))
                .update(data.toJSON())
                .then((r) => {
                    let pos = this.list.findIndex((value) => value.matricule == data.matricule);
                    if (pos >= 0) this.list[pos] = data;
                    let result = new ActionResult<boolean>();
                    result.result = true;
                    resolve(result)
                })
                .catch((error) => {
                    console.log("Error ", error)
                    let r = new ActionResult<boolean>()
                    r.result = false;
                    reject(r)
                })

        
        })
    }

    delete(data: Patient): Promise<ActionResult<boolean>> {
        return new Promise<ActionResult<boolean>>((resolve, reject) => {
            this.db(this.tablename)
                .where((builder) => builder.where({ "matricule": data.matricule }))
                .delete()
                .then((r) => {
                    let pos = this.list.findIndex((value) => value.matricule == data.matricule);
                    if (pos >= 0) this.list.splice(pos, 1);

                    let result = new ActionResult<boolean>();
                    result.result = true;
                    resolve(result)
                })
                .catch((error) => {
                    console.log("Error ", error)
                    let r = new ActionResult<boolean>()
                    r.result = false;
                    reject(r)
                })

        })

    }
}
