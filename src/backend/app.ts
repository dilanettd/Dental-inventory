import { StartService } from "./services/StartService"
import { InjectorContainer } from "./utils/lifecycle/injector_container"
import * as bussiness from "./bussiness"

let startService:StartService = new StartService()
startService.init()

let injector = InjectorContainer.getInstance()


injector.saveInstance<StartService>(StartService,startService)


bussiness.initAuthEvent()
bussiness.initConsultationEvent()
bussiness.initSoinsEvent()
bussiness.initPatientEvent()
bussiness.initItemEvent()
bussiness.initHistoryEvent()



