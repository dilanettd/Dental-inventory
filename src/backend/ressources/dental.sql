-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.2.3-MariaDB-log - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour hostoinventory
CREATE DATABASE IF NOT EXISTS `hostoinventory` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hostoinventory`;

-- Export de la structure de la table hostoinventory. action
CREATE TABLE IF NOT EXISTS `action` (
  `idAction` int(11) NOT NULL AUTO_INCREMENT,
  `idProduit` int(11) NOT NULL,
  `type` char(50) NOT NULL,
  `quantite` int(11) NOT NULL,
  `jour` char(50) NOT NULL,
  `moi` char(50) NOT NULL,
  `annee` char(50) NOT NULL,
  PRIMARY KEY (`idAction`),
  KEY `FK__produit` (`idProduit`),
  CONSTRAINT `FK__produit` FOREIGN KEY (`idProduit`) REFERENCES `produit` (`idProduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table hostoinventory. consultation
CREATE TABLE IF NOT EXISTS `consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matricule` char(50) NOT NULL,
  `idSoins` int(11) NOT NULL,
  `jour` char(50) NOT NULL,
  `moi` char(50) NOT NULL,
  `annee` char(50) NOT NULL,
  `prix` int(11) NOT NULL,
  PRIMARY KEY (`idConsultation`),
  KEY `FK__patient` (`matricule`),
  KEY `FK__soins` (`idSoins`),
  CONSTRAINT `FK__patient` FOREIGN KEY (`matricule`) REFERENCES `patient` (`matricule`),
  CONSTRAINT `FK__soins` FOREIGN KEY (`idSoins`) REFERENCES `soins` (`idSoins`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table hostoinventory. patient
CREATE TABLE IF NOT EXISTS `patient` (
  `matricule` char(50) NOT NULL,
  `nom` char(50) NOT NULL,
  `prenom` char(50) DEFAULT NULL,
  `age` char(50) DEFAULT NULL,
  `lieuN` char(50) DEFAULT NULL,
  `telephone` char(50) DEFAULT NULL,
  `mail` char(50) DEFAULT NULL,
  `sexe` char(50) DEFAULT NULL,
  `adresse` char(50) DEFAULT NULL,
  `typeClient` char(50) NOT NULL,
  PRIMARY KEY (`matricule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table hostoinventory. produit
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` char(50) NOT NULL,
  `nomCommercial` char(50) NOT NULL,
  `image` char(50) NOT NULL,
  `quantite` int(11) NOT NULL,
  `datePeremption` char(50) NOT NULL,
  `conditionnement` char(50) NOT NULL,
  `fabricant` char(50) NOT NULL,
  PRIMARY KEY (`idProduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table hostoinventory. soins
CREATE TABLE IF NOT EXISTS `soins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` char(50) NOT NULL,
  `description` text DEFAULT NULL,
  `prixContoire` int(11) NOT NULL,
  `prixAssurer` int(11) NOT NULL,
  `prixPersonnel` int(11) NOT NULL,
  PRIMARY KEY (`idSoins`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table hostoinventory. utilisateur
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` char(50) NOT NULL,
  `password` char(50) NOT NULL,
  `nom` char(50) DEFAULT NULL,
  `typeUser` char(50) DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Les utilisateurs de l''application';

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
