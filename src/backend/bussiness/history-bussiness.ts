import { ipcMain } from "electron";
import { TypeAction } from "../../shared/enums";
import { ConsultationService, HistoriqueService } from "../services";

import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";


function countTodayReleasedProduct({ day, month, year })
{
    const historyInstance = InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService)

    return historyInstance.getListObjectsByJour(year, month, day).filter((action) => action.getTypeAction() == TypeAction.DELETE_ACTION).length
}

function hanldeListReleaseProductByDate({ day, month, year })
{
    const historyInstance = InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService)

    return historyInstance.getListObjectsByJour(year, month, day).filter((action) => action.getTypeAction() == TypeAction.DELETE_ACTION)

}

function hanldeListAllProduct()
{
    const historyInstance = InjectorContainer.getInstance().getInstanceOf<HistoriqueService>(HistoriqueService)

    return historyInstance.getList()
}

export function initHistoryEvent()
{
    ipcMain.handle('list-today-product-release-event', (event, arg) => hanldeListReleaseProductByDate(arg))
    ipcMain.handle('count-today-product-release-event',(event,arg)=>countTodayReleasedProduct(arg))
    ipcMain.handle('list-product-event', (event, arg) => hanldeListAllProduct())

}