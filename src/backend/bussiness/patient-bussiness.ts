import { ipcMain } from "electron";
import { TypeAction } from "../../shared/enums";
import { Patient, Soins } from "../../shared/model";
import { PatientService, SoinsService } from "../services";

import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";


function getPatientList()
{
    let patientServiceInstance = InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService)

    return patientServiceInstance.getList()
}

function handleOnUpdatePatient(s)
{
    let patientServiceInstance = InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService)

    return patientServiceInstance.update(new Patient(s.matricule,s.firstname,s.lastname,s.datenaiss,s.placenaiss,s.tel,s.email,s.adress,s.typePatient,s.sexe))
}

function handleOnDeletePatient(matricule)
{
    let patientServiceInstance = InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService)

    let patient = patientServiceInstance.findByField("matricule",matricule)
    if(patient.length>0) return patientServiceInstance.delete(patient[0])
    return new ActionResult(ActionResult.INVALID_ARGUMENT,"error","",false)
}
function handleOnAddPatientEvent(s)
{
    let patientServiceInstance = InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService)

    let patient =new Patient(Patient.generatedMatricule(),s.firstname,s.lastname,s.datenaiss,s.placenaiss,s.tel,s.email,s.adress,s.typePatient,s.sexe)
    patientServiceInstance.save(patient)
    let result=new ActionResult()
    result.result={
        ok:true,
        obj:patient
    }
    return result
}

export function initPatientEvent()
{
    ipcMain.handle('update-customer-event',(event,arg)=>handleOnUpdatePatient(arg))
    ipcMain.handle('list-patient-event',(event,arg)=>getPatientList())
    ipcMain.handle('delete-customer-matricule',(event,arg)=>handleOnDeletePatient(arg))
    ipcMain.handle('add-customer-event',(event,arg)=>handleOnAddPatientEvent(arg))
}
