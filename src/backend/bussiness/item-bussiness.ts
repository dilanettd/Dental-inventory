import { ipcMain } from "electron"
import { Entity, Produit } from "../../shared/model"
import { StockService } from "../services"
import { ActionResult } from "../utils/ActionResult"
import { InjectorContainer } from "../utils/lifecycle/injector_container"



function getItemList()
{
    let stockServiceInstance = InjectorContainer.getInstance().getInstanceOf<StockService>(StockService)

    return stockServiceInstance.getList()
}

function handleOnUpdateItem(s)
{
    let stockServiceInstance = InjectorContainer.getInstance().getInstanceOf<StockService>(StockService)

    return stockServiceInstance.update(new Produit(s.id,s.nom,s.nomCommercial,"",parseInt(s.quantite),s.conditionnement,s.datePeremption,s.fabricant))
}


function updateQuantiti(s)
{
    let stockServiceInstance = InjectorContainer.getInstance().getInstanceOf<StockService>(StockService)

    let produit = stockServiceInstance.findByField("id",s.id)
    return stockServiceInstance.changeQuantiti(produit[0],parseInt(s.quantite),s.action)
}

function handleOnAddProduitEvent(s)
{
    let stockServiceInstance = InjectorContainer.getInstance().getInstanceOf<StockService>(StockService)

    let produit = new Produit(Entity.generatedID(),s.nom,s.nomCommercial,"",0,s.conditionnement,s.datePeremption,s.fabricant)
    stockServiceInstance.save(produit)
    let result=new ActionResult()
    result.result={
        ok:true,
        obj:produit
    }
    return result
}


export function initItemEvent()
{
    ipcMain.handle('update-item-event',(event,arg)=>handleOnUpdateItem(arg))
    ipcMain.handle('list-items-event',(event,arg)=>getItemList())
    ipcMain.handle('update-item-quantiti',(event,arg)=>updateQuantiti(arg))
    ipcMain.handle('add-item-event',(event,arg)=>handleOnAddProduitEvent(arg))
}