import { ipcMain } from "electron";
import { Consultation, Entity } from "../../shared/model";
import { ConsultationService, PatientService, SoinsService } from "../services";

import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";


function handleOnListConsultation()
{
    const consultationInstance = InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService)

    return consultationInstance.getList()
}

function handleOnTodayConsultationEvent({day,month,year})
{
    const consultationInstance = InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService)

    return consultationInstance.getNumberObjectByDay(year,month,day)
}
function listToDayConsultationEvent({day,month,year})
{
    const consultationInstance = InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService)

    return consultationInstance.getListObjectsByJour(year,month,day)
}

function handleOnDeleteConsultation(id)
{
    const consultationInstance = InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService)

    let consultation = consultationInstance.findByField("id",id)
    if(consultation.length>0) return consultationInstance.delete(consultation[0])
    return new ActionResult(ActionResult.INVALID_ARGUMENT,"error","",false)
}
function handleOnAddConsultationEvent(data)
{
    const consultationInstance = InjectorContainer.getInstance().getInstanceOf<ConsultationService>(ConsultationService)
    const patientInstance = InjectorContainer.getInstance().getInstanceOf<PatientService>(PatientService)
    const soinsInstance = InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService)

    let soins = data.soins.map((s)=> soinsInstance.findByField("id",s)[0])
    let consultation:Consultation=new Consultation(
        Entity.generatedID(),
        data.prix,
        `${(new Date(data.date_consultation)).getDate()}`,
        `${(new Date(data.date_consultation)).getMonth()+1}`,
        `${(new Date(data.date_consultation)).getFullYear()}`,
        patientInstance.findByField("matricule",data.matricule_customer)[0],
        soins,
        data.comment
    )
    consultationInstance.save(consultation)
    let result=new ActionResult()
    result.result={
        ok:true,
        obj:consultation
    }
    return result
}

export function initConsultationEvent()
{
    ipcMain.handle("list-consultation-event",(event,arg)=> handleOnListConsultation())
    ipcMain.handle("list-today-consultation-event",(event,arg)=> listToDayConsultationEvent(arg))

    ipcMain.handle("count-todayconsultation-event",(event,arg)=> handleOnTodayConsultationEvent(arg))
    ipcMain.handle("delete-consultation-by-id",(event,arg)=>handleOnDeleteConsultation(arg))
    ipcMain.handle("add-consultation-event",(event,data)=>handleOnAddConsultationEvent(data))

    // ipcMain.handle("list-consultation-by-date")
}