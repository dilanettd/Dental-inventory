import { ipcMain } from "electron";
import { AuthService } from "../services/AuthService";
import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";



export function handleOnLoginEvent({login,password}):Promise<ActionResult<boolean>>
{
    let authServiceInstance = InjectorContainer.getInstance().getInstanceOf<AuthService>(AuthService)

    return authServiceInstance.login(login,password)
}   
export function handleOnLogoutEvent()
{
    let authServiceInstance = InjectorContainer.getInstance().getInstanceOf<AuthService>(AuthService)

    return authServiceInstance.logout()
}

export function initAuthEvent()
{
    ipcMain.handle("login-event",(event,arg)=> handleOnLoginEvent(arg))
    ipcMain.handle("logout-event",(event,arg)=> handleOnLogoutEvent())
}