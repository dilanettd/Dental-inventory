import { ipcMain } from "electron";
import { TypeAction } from "../../shared/enums";
import { Entity, Soins } from "../../shared/model";
import { SoinsService } from "../services";

import { ActionResult } from "../utils/ActionResult";
import { InjectorContainer } from "../utils/lifecycle/injector_container";


function handleOnListSoins()
{
    const soinsInstance = InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService)

    return soinsInstance.getList()
}

function handleOnUpdateSoins(s)
{
    const soinsInstance = InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService)

    return soinsInstance.update(new Soins(s.id,s.nom,s.description,s.prixContoire,s.prixAssurer,s.prixPersonnel))
}


function handleOnDeleteSoins(id)
{
    const soinsInstance = InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService)

    let soins = soinsInstance.findByField("id",id)
    if(soins.length>0) return soinsInstance.delete(soins[0])
    return new ActionResult(ActionResult.INVALID_ARGUMENT,"error","",false)
}

function handleOnAddSoins(data)
{
    const soinsInstance = InjectorContainer.getInstance().getInstanceOf<SoinsService>(SoinsService)

    let soins=new Soins(Entity.generatedID(),data.name,data.description,data.standart_price,data.insured_price,data.staff_price)
    soinsInstance.save(soins)
     let result=new ActionResult()
    result.result={
        ok:true,
        obj:soins
    }
    return result

}

export function initSoinsEvent()
{
    ipcMain.handle('list-soins-event',(event,arg)=>handleOnListSoins())
    ipcMain.handle('update-soins-item-event',(event,arg)=>handleOnUpdateSoins(arg))
    ipcMain.handle('delete-soins-by-id',(event,arg)=>handleOnDeleteSoins(arg))
    ipcMain.handle('add-soins-event',(event,arg)=>handleOnAddSoins(arg))

}