import {  initAuthEvent } from "./auth-bussiness";
import { initConsultationEvent } from "./consultation-bussiness";
import { initSoinsEvent } from "./soins-bussiness";
import { initHistoryEvent } from "./history-bussiness";
import { initPatientEvent } from "./patient-bussiness";
import { initItemEvent } from "./item-bussiness";


export
{
    initAuthEvent,
    initConsultationEvent,
    initHistoryEvent,
    initSoinsEvent,
    initPatientEvent,
    initItemEvent
}