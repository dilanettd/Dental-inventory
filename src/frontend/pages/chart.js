const systolic = document.getElementById('systolicu');
const diastolic = document.getElementById('diastolicu');
const armLocation = document.getElementById('armLocationu');
const bodyPosition = document.getElementById('bodyPositionu')
const pulse = document.getElementById('pulseu')
const datetime = document.getElementById('datetimeu')
const notes = document.getElementById('notesu')
const id = document.getElementById('idu')
const formAddMeasurement = document.getElementById('formAddMeasurement')

$(document).ready(function () {
    $('.floatingButton').on('click',
        function (e) {
            e.preventDefault();
            $(this).toggleClass('open');
            $('.floatingMenu').stop().slideToggle();
            $(this).hide()

        }
    );
});

$("#myModal1").on("hidden.bs.modal", function () {
    refreshData();
    formAddMeasurement.reset()
    $('.floatingButton').show()
});
$("#myModal").on("hidden.bs.modal", function () {
    refreshData();
    $('.floatingButton').show()
});

function displayModal(data) {
    console.log("Data", data)
    pulse.value = data.pulse
    systolic.value = data.systolic
    diastolic.value = data.diastolic
    armLocation.value = data.armLocation
    bodyPosition.value = data.bodyPosition
    notes.value = data.notes
    datetime.value = data.date + 'T' + data.time
    id.value = data.id
    $('.floatingButton').hide()
}

function tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}

const DateMessure =
    {
        day: "day",
        weekly: "week",
        month: "month",
        year: "year"
    }
let dayOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
let monthOfYear = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
let getMeasureTable = [];

class CustomData {
    constructor() {
        this.date = new Date();
        this.currentDateValue = this.getDailyDate(new Date());
        this.typemessure = DateMessure.day;
        this.data = []
        this.currentData = [];
    }

    setData(data) {
        this.data = data;
    }

    previous() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() - 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() - 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() - 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() - 1)
                break
        }
    }

    next() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() + 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() + 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() + 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() + 1)
                break
        }
    }

    getLabels() {
        switch (this.typemessure) {
            case DateMessure.day:
                return this.currentData.map((data) => tConvert(data.time));
            case DateMessure.weekly:
                return this.currentData.map((date) => dayOfWeek[(new Date(date.key)).getDay()]);
            case DateMessure.month:
                return this.currentData.map((date) => `${date.key.split("_")[0].split("-")[2]} - ${date.key.split("_")[1].split("-")[2]}`);
            case DateMessure.year:
                return this.currentData.map((data) => monthOfYear[data.key])
        }
    }

    applyFilter() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.currentData = this.data.filter((data) => data.date == this.getDailyDate(this.date));
                return this.currentData;
                console.log("currentData", this.currentData)
            case DateMessure.weekly:
                this.currentData = new Map();
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                this.data.forEach((data) => {
                    let currDate = new Date(data.date);
                    if (currDate.getFullYear() == this.date.getFullYear() && currDate.getMonth() == this.date.getMonth()) {
                        if (firstDayOfWeek <= currDate && lastDayOfWeek >= currDate) {
                            if (this.currentData.has(data.date)) this.currentData.get(data.date).push(data);
                            else this.currentData.set(data.date, [data])
                        }
                    }

                })
                getMeasureTable = [];
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                return getMeasureTable;
            case DateMessure.month:
                getMeasureTable = [];
                let firstDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
                let lastDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
                this.currentData = new Map();
                let i = firstDayOfMonth.getDate();
                while (i <= lastDayOfMonth.getDate()) {
                    this.data.forEach((data) => {
                        let currDate = new Date(data.date);
                        if (currDate.getFullYear() == firstDayOfMonth.getFullYear() && currDate.getMonth() == firstDayOfMonth.getMonth()) {
                            if (new Date(currDate.getFullYear(), currDate.getMonth(), i) <= currDate && new Date(currDate.getFullYear(), currDate.getMonth(), i + 6) >= currDate) {
                                let k = new Date(currDate.getFullYear(), currDate.getMonth(), i);
                                let k1 = new Date(currDate.getFullYear(), currDate.getMonth(), i + 6);
                                if (this.currentData.has(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`)) this.currentData.get(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`).push(data);
                                else this.currentData.set(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`, [data])
                            }
                        }
                    })
                    i += 7;
                }
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                return getMeasureTable;
            case DateMessure.year:
                getMeasureTable = [];
                this.currentData = new Map();
                this.data.forEach((data) => {
                    let currDate = new Date(data.date);
                    if (currDate.getFullYear() == this.date.getFullYear()) {
                        if (this.currentData.has(currDate.getMonth())) this.currentData.get(currDate.getMonth()).push(data)
                        else this.currentData.set(currDate.getMonth(), [data])
                    }
                })
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                return getMeasureTable;
        }
    }

    generateData() {
        switch (this.typemessure) {
            case DateMessure.month:
            case DateMessure.year:
            case DateMessure.weekly:
                let tabData = [];
                Array.from(this.currentData.keys()).forEach((key) => {
                    let itemResult = this.currentData.get(key).reduce((acc, curr) => {
                        return {
                            "systolic": parseInt(acc.systolic) + parseInt(curr.systolic),
                            "diastolic": parseInt(acc.diastolic) + parseInt(curr.diastolic),
                            "pulse": parseInt(acc.pulse) + parseInt(curr.pulse),
                            "key": acc.key
                        }
                    }, {
                        "systolic": 0,
                        "diastolic": 0,
                        "pulse": 0,
                        key
                    })
                    itemResult.systolic = Math.round(itemResult.systolic / this.currentData.get(key).length);
                    itemResult.diastolic = Math.round(itemResult.diastolic / this.currentData.get(key).length);
                    itemResult.pulse = Math.round(itemResult.pulse / this.currentData.get(key).length);
                    tabData.push(itemResult)
                })
                if (this.typemessure == DateMessure.weekly) tabData.sort((dataA, dataB) => new Date(dataA.key) <= new Date(dataB.key) ? -1 : +1)
                if (this.typemessure == DateMessure.month) tabData.sort((dataA, dataB) => new Date(dataA.key.split("_")[0]) <= new Date(dataB.key.split("_")[0]) ? -1 : +1)

                //tabData.forEach((data)=> delete data.key);
                this.currentData = tabData.slice();
        }
        return {
            "label": this.getLabels(),
            "systolic": this.currentData.map((data) => data.systolic),
            "diastolic": this.currentData.map((data) => data.diastolic),
            "pulse": this.currentData.map((data) => data.pulse)
        }
    }

    showData() {
        this.applyFilter()
        return this.generateData()
    }

    getDailyDate(date) {
        let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        return `${date.getFullYear()}-${date.getMonth() + 1}-${day}`
    }

    getLabel() {
        switch (this.typemessure) {
            case DateMessure.day:
                return this.date.getDate() + '-' + monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear();
            case DateMessure.weekly:
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                // return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${firstDayOfWeek.getMonth() + 1 < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : firstDayOfWeek.getMonth() + 1}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + (lastDayOfWeek.getDate()+1) : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth()+1 < 10 ? "0" + (firstDayOfWeek.getMonth()+1) : firstDayOfWeek.getMonth()+1}/${firstDayOfWeek.getFullYear()}`
                return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${(firstDayOfWeek.getMonth() + 1) < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : (firstDayOfWeek.getMonth() + 1)}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + lastDayOfWeek.getDate()  : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth() + 1 < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : firstDayOfWeek.getMonth() + 1}/${firstDayOfWeek.getFullYear()}`

            case DateMessure.month:
                return monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear()
            case DateMessure.year:
                return this.date.getFullYear()
        }
    }
}


let customData = new CustomData();

//Static param
Chart.register(ChartDataLabels);
Chart.defaults.set('plugins.datalabels', {
    color: '#black',
    anchor: 'end',
    align: 'top'
});
Chart.defaults.set('plugins.legend', {
    display: true,
    position: "bottom"
});

var canvas = document.getElementById("canvas")
var ctx = canvas.getContext("2d");
window.myBar = new Chart(ctx, {
    type: "bar",
});

///End static param


function showChart(data) {
    var barChartData = {
        labels: data.label,
        datasets: [
            {
                label: "Systole",
                backgroundColor: "red",
                borderWidth: 1,
                data: data.systolic,
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30
            },
            {
                label: "Diastole",
                backgroundColor: "blue",
                borderWidth: 1,
                data: data.diastolic,
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30
            },
        ]
    }
    var chartOptions = {
        responsive: true,
        legend: {
            display: 'false',
            position: 'bottom'
        },
        scales: {
            y: {
                title: {
                    display: true,
                    text: 'Systolic/Diastolic(mmHg)'
                },
                max: 240,
                min: 0,
                ticks: {
                    stepSize: 40
                }
            }
        }
    }
    myBar.clear();
    myBar.data.datasets = barChartData.datasets
    myBar.data.labels = barChartData.labels
    myBar.options = chartOptions
    myBar.update()
    document.querySelector("#label").textContent = customData.getLabel();


}

// Pulse chart

function showChart1(data) {
    var barChartData = {
        labels: data.label,
        datasets: [
            {
                label: "Pulse",
                backgroundColor: "green",
                borderWidth: 1,
                data: data.pulse,
                barThickness: 25,
                maxBarThickness: 40,
                minBarLength: 30
            },
        ]
    }
    var chartOptions = {
        responsive: true,
        legend: {
            display: 'true',
            position: 'bottom'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                title: {
                    display: true,
                    text: 'Pulse(bpm)'
                },
                max: 160,
                min: 0,
                ticks: {
                    stepSize: 40
                }
            }],
        }
    }
    myBar.clear();
    myBar.data.datasets = barChartData.datasets
    myBar.data.labels = barChartData.labels
    myBar.options = chartOptions
    myBar.update()
    document.querySelector("#label").textContent = customData.getLabel();
}

// tableData
function loadTableData(tableData) {
    const tableBody = document.getElementById('tableData');
    tableBody.innerHTML = ""
    let color;
    for (let data of tableData.reverse()) {
        // Color test
        if (data.systolic <= 120 && data.diastolic <= 80) {
            color = '#04A141'
        } else if (data.systolic <= 130 && data.diastolic <= 85) {
            color = '#84f107'
        } else if (data.systolic <= 140 && data.diastolic <= 90) {
            color = '#f1e507'
        } else if (data.systolic <= 160 && data.diastolic <= 100) {
            color = 'rgba(238,54,10,0.94)'
        } else {
            color = 'rgb(248,7,7)'
        }
        let trObj = document.createElement("tr");
        trObj.setAttribute("data-bs-toggle", "modal")
        trObj.setAttribute("href", "#myModal")
        trObj.setAttribute("style", "cursor: pointer;")
        trObj.innerHTML = `<td id="classification" style="background-color: ${color} ; border-radius: 10px; width : 1px"></td> 
                            <td class="text-center">${data.date}</td>
                            <td class="text-center"> (  ${data.systolic} | ${data.diastolic})</td>
                            <td class="text-center"> ${data.pulse}</td>
                            <td> <i class="fa fa-chevron-right"/> </td>`
        trObj.addEventListener("click", (e) => displayModal(data))
        tableBody.append(trObj)
    }
    //tableBody.innerHTML = dataHtml;

}

currentChart = "dia/sys";
var getData = $.get('/chart');
getData.done(function (data) {
    customData.setData(data.measurement)
    loadTableData(customData.applyFilter())
    if (currentChart === "dia/sys") {
        showChart(customData.showData())
    } else if (currentChart === "pulse") {
        showChart1(customData.showData())
    }
});

//event listener
document.querySelector("#previous_btn").addEventListener('click', (event) => {
    customData.previous()
    loadTableData(customData.applyFilter())
    if (currentChart === "dia/sys") {
        showChart(customData.showData())
    } else if (currentChart === "pulse") {
        showChart1(customData.showData())

    }
})
document.querySelector("#next_btn").addEventListener('click', (event) => {
    customData.next()
    loadTableData(customData.applyFilter())
    if (currentChart === "dia/sys") {
        showChart(customData.showData())
    } else if (currentChart === "pulse") {
        showChart1(customData.showData())

    }
})
// Radio group day
let radios = document.querySelectorAll('input[type=radio][name="switch"]');
radios.forEach(radio => radio.addEventListener('change', (e) => {
    customData.typemessure = e.currentTarget.value;
    loadTableData(customData.applyFilter())
    if (currentChart === "dia/sys") {
        showChart(customData.showData())
    } else if (currentChart === "pulse") {
        showChart1(customData.showData())
    }
}));

// Radio group typ
let radios1 = document.querySelectorAll('input[type=radio][name="switch1"]');
radios1.forEach(radio => radio.addEventListener('change', (e) => {
    currentChart = e.currentTarget.value;
    loadTableData(customData.applyFilter())
    if (currentChart === "dia/sys") {
        showChart(customData.showData())
    } else if (currentChart === "pulse") {
        showChart1(customData.showData())
    }
}));

function refreshData() {
    var getData = $.get('/chart');
    getData.done(function (data) {
        customData.setData(data.measurement)
        loadTableData(customData.applyFilter())
        if (currentChart === "dia/sys") {
            showChart(customData.showData())
        } else if (currentChart === "pulse") {
            showChart1(customData.showData())
        }
    });
}

/// Manage measurement
$(function () {
    $('#addMeasurement').click(function () {
        $.ajax({
            type: 'POST',
            url: '/dashboard',
            data: {
                datetime: $('#datetime').val(),
                pulse: $('#pulse').val(),
                notes: $('#notes').val(),
                armLocation: $('#armLocation').val(),
                bodyPosition: $('#bodyPosition').val(),
                diastolic: $('#diastolic').val(),
                systolic: $('#systolic').val(),
                measurement: "measurement",
            },

            success: function (response) {
                console.log(response);
            },
            erreur: function (erreur) {
                console.log(erreur);
            }

        });
    });
    $('#deleteMeasurement').click(function () {
        if (confirm("Are you sure you want to remove this measure?")) {
            $.ajax({
                type: 'POST',
                url: '/dashboard',
                data: {
                    idMeasurement: $('#idu').val(),
                    deleteMeasurement: "deleteMeasurement",
                },

                success: function (response) {
                    console.log(response);
                },
                erreur: function (erreur) {
                    console.log(erreur);
                }

            });
        }
    });
    $('#updateMeasurement').click(function () {
        if (confirm("Are you sure you want to edit the notes for this measure?")) {
            $.ajax({
                type: 'POST',
                url: '/dashboard',
                data: {
                    idMeasurement: $('#idu').val(),
                    notes: $('#notesu').val(),
                    updateMeasurement: "updateMeasurement",
                },

                success: function (response) {
                    console.log(response);
                },
                erreur: function (erreur) {
                    console.log(erreur);
                }

            });
        }
    });
});

