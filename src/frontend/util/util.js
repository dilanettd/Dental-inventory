const notifier = new AWN();
const store = new Store()
store.load()

const monthList=[
                    "january",
                    "febuary",
                    "march",
                    "april",
                    "may",
                    "june",
                    "jully",
                    "august",
                    "september",
                    "october",
                    "november",
                    "december"
    ];

const retMonthList=[
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dev"
    ];

class DateUtil {
    static getTodayDay()
    {
    }
    static getTodayMonth()
    {
    }
    static getToDayRetMonth()
    {
        return retMonthList[new Date().getMonth()]
    }
    static getTodayYear()
    {
    }

    static getMonthPosition(month)
    {
        
    }
}

function apply(target)
{
    target.show=(disp)=>{
        target.dataset.display=`${disp}`
    }
    target.text=(t)=>{
        if(t) target.textContent=t;
        else return target.textContent 
    }
    
    return target
}



function el(str)
{
    return apply(document.querySelector(str))
}

function els(str)
{
    return document.querySelectorAll(str)
}

function handleOnLogoutEvent()
{
    els("[data-event='logout']").forEach(()=>{
        ipcRenderer.send('logout-event', "")
    })
}

function showToDayDate()
{
    els("[data-show='today-date']")
    .forEach((e)=>apply(e).text(`${new Date().getDate()<10?"0"+new Date().getDate():new Date().getDate()}-${DateUtil.getToDayRetMonth()}-${new Date().getFullYear()}`))
}

function showDataInCondition()
{
    els("[data-if]").forEach((element)=>{
        let exp=element.dataset.if;
        let result = eval(exp)
        if(result==false)  element.style.display="none";
    })
}

function display(target,dis)
{
    target.style.visibility=dis==true?"visible":"hidden"
}

function displayIf()
{
    els("[data-display]").forEach((element)=>{
        display(element,element.dataset.display==="true")
        let observer = new MutationObserver((mutations,obs)=>{
            for( const mutation of mutations)
            {
                if(mutation.type=="attributes" && mutation.attributeName=="data-display" )
                {
                   display(mutation.target,mutation.target.dataset.display==="true")
                }
            }
        });
        observer.observe(element,{attributes:true,childList:false,subtree:false})
    })

}







(function(){
    handleOnLogoutEvent()
    showToDayDate()
    showDataInCondition()
    displayIf()
})()