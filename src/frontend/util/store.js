class Store
{
    constructor()
    {
        this._store = new Map();
    }

    get(key)
    {
        this._store.get(key)
    }
    put(key,value)
    {
        this._store.set(key,value)
    }
    persist()
    {
        this._store.forEach((value,key)=>{
            localStorage.setItem(key,JSON.stringify(value))
        })
    }
    load()
    {
        for ( let i = 0; i < localStorage.length;  ++i ) {
            let key = localStorage.key( i )
            this.persist(key, JSON.parse(localStorage.get(key)))            
        }
    }
} 