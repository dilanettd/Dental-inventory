// console.log(window)
var val  = 2
// console.log($)
el("[type='submit']").addEventListener('click', (e) =>
    {
        e.preventDefault()
        let login = el("#login").value, password=el("#password").value;
        if(login.length==0 || password.length==0) return notifier.warning("Credential cannot empty")
        el("#submit-spinner").show(true)
        el("#text-submit").text("Patientez...") 
        e.currentTarget.disabled=true
        ipcRenderer
        .invoke('login-event', {login,password})
        .then((result) =>{
            // console.log("result",result)
          if(result.result==true)
          {
            notifier.success("Success authentification ")
            setTimeout(()=> window.location.href="./../pages/home.html",500)
          }
          else 
          {
              el("#submit-spinner").show(false)
              el("#text-submit").text("Login")
              el("#password").value=""
              el("#login").value=""
              e.target.disabled=false
              notifier.alert("Invalid credentials")
          }
        })
    }
)

