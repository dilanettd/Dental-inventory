let dayOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
let monthOfYear = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
let customData=null
let soinsList = store.get("soins")


const DateMessure =
{
    day: "day",
    weekly: "week",
    month: "month",
    year: "year"
}

class CustomData {
    constructor() {
        this.date = new Date();
        this.currentDateValue = this.getDailyDate(new Date());
        this.typemessure = DateMessure.weekly;
        this.data = []
        this.selectedFilter = []
        this.currentData = [];
    }

    setData(data) {
        
        this.data = data.map((d) => {
            // console.log("D",d)
                d["date"]= `${parseInt(d.jour) < 10 ? "0" + d.jour : d.jour}/${parseInt(d.moi) < 10 ? "0" + d.moi : d.moi}/${d.annee}`
                return d
            }
        );
        console.log("data ",this.data)
    }
    setTypeMesure(type)
    {
        this.typemessure=type
    }
    setTextFilter(filter)
    {
        this.selectedFilter=filter
    }

    previous() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() - 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() - 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() - 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() - 1)
                break
        }
    }

    next() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() + 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() + 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() + 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() + 1)
                break
        }
    }

    getLabels() {
        let labels = []
        switch (this.typemessure) {
            case DateMessure.day:
                return this.currentData.map((data) => data.date);
            case DateMessure.weekly:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(dayOfWeek[(new Date(parseInt(date.split("/")[2]), parseInt(date.split("/")[1]) - 1, parseInt(date.split("/")[0]))).getDay()])
                });
                return labels
            case DateMessure.month:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(`${date.split("_")[0].split("/")[0]} - ${date.split("_")[1].split("/")[0]}`)
                });
                return labels
            case DateMessure.year:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(monthOfYear[date])
                });
                return labels
        }

    }
    textFilter()
    {
        console.log("currentData ", this.currentData)

        if(this.selectedFilter.length==0) return;
        Array.from(this.currentData.keys()).forEach((key)=>{
            this.currentData.set(key,this.currentData.get(key).filter((consult)=>{
                let exist=false;
                this.selectedFilter.forEach((filter) => {
                    if(consult.soins.map((soin) => soin.id).includes(filter)) exist=true
                })
                return exist                
            }))
        })
    }
    applyFilter() {
        let getMeasureTable = [];
        switch (this.typemessure) {
            case DateMessure.day:
                this.currentData = this.data.filter((data) => {
                    return data.date == this.getDailyDate(this.date)
                });
                this.textFilter()
                return this.currentData;
            case DateMessure.weekly:
                this.currentData = new Map();
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                this.data.forEach((data) => {
                    let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                    if (currDate.getFullYear() == this.date.getFullYear() && currDate.getMonth() == this.date.getMonth()) {
                        if (firstDayOfWeek <= currDate && lastDayOfWeek >= currDate) {
                            if (this.currentData.has(data.date)) this.currentData.get(data.date).push(data);
                            else this.currentData.set(data.date, [data])
                        }
                    }

                })
                getMeasureTable = []
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
            case DateMessure.month:
                getMeasureTable = []
                let firstDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
                let lastDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
                this.currentData = new Map();
                let i = firstDayOfMonth.getDate();
                while (i <= lastDayOfMonth.getDate()) {
                    this.data.forEach((data) => {
                        let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                        if (currDate.getFullYear() == firstDayOfMonth.getFullYear() && currDate.getMonth() == firstDayOfMonth.getMonth()) {
                            if (new Date(currDate.getFullYear(), currDate.getMonth(), i) <= currDate && new Date(currDate.getFullYear(), currDate.getMonth(), i + 6) >= currDate) {
                                let k = new Date(currDate.getFullYear(), currDate.getMonth(), i);
                                let k1 = new Date(currDate.getFullYear(), currDate.getMonth(), i + 6);
                                if (this.currentData.has(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`)) this.currentData.get(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`).push(data);
                                else this.currentData.set(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`, [data])
                            }
                        }
                    })
                    i += 7;
                }
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
            case DateMessure.year:
                getMeasureTable = [];
                this.currentData = new Map();
                this.data.forEach((data) => {
                    let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                    if (currDate.getFullYear() == this.date.getFullYear()) {
                        if (this.currentData.has(currDate.getMonth())) this.currentData.get(currDate.getMonth()).push(data)
                        else this.currentData.set(currDate.getMonth(), [data])
                    }
                })

                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
        }
    }

    generateData() {
        let tabData=[]
        switch (this.typemessure) {
            case DateMessure.month:
            case DateMessure.year:
            case DateMessure.weekly:
                tabData = [];
                console.log("value",this.currentData)
                Array.from(this.currentData.keys()).forEach((key) => {
                    let itemResult = this.currentData.get(key).map((consult)=>consult.prix).reduce((acc, curr) => parseInt(curr)+acc,0)
                    
                    tabData.push({
                        "nbconsultation": this.currentData.get(key).length,
                        "price":itemResult
                    })
                })
                // if (this.typemessure == DateMessure.weekly) tabData.sort((dataA, dataB) => new Date(dataA.key) <= new Date(dataB.key) ? -1 : +1)
                // if (this.typemessure == DateMessure.month) tabData.sort((dataA, dataB) => new Date(dataA.key.split("_")[0]) <= new Date(dataB.key.split("_")[0]) ? -1 : +1)

                //tabData.forEach((data)=> delete data.key);
                break
               
        }
        // this.currentData = tabData.slice();
        return {
            "label": this.getLabels(),
            "data": [...tabData]
        }
    }

    showData() {
        this.applyFilter()
        return this.generateData()
    }

    getDailyDate(d) {
        // let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        // return `${date.getFullYear()}-${date.getMonth() + 1}-${day}`
        return `${parseInt(d.getDate()) < 10 ? "0" + d.getDate() : d.getDate()}/${parseInt(d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : d.getMonth()+1}/${d.getFullYear()}`
    }

    getLabel() {
        switch (this.typemessure) {
            case DateMessure.day:
                return this.date.getDate() + '-' + monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear();
            case DateMessure.weekly:
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                // return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${firstDayOfWeek.getMonth()}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + lastDayOfWeek.getDate() : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth()}/${firstDayOfWeek.getFullYear()}`
                return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${(firstDayOfWeek.getMonth() + 1) < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : (firstDayOfWeek.getMonth() + 1)}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + lastDayOfWeek.getDate() : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth() + 1 < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : firstDayOfWeek.getMonth() + 1}/${firstDayOfWeek.getFullYear()}`

            case DateMessure.month:
                return monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear()
            case DateMessure.year:
                return this.date.getFullYear()
        }
    }
}
function setSelectSoinsComponents() {
    let selectListSoins = document.querySelector("#list-of-treatement")
    selectListSoins.innerHTML = "";
    for (var i = 0; i < soinsList.length; i++) {
        var option = document.createElement("option");
        option.value = soinsList[i].id;
        option.text = soinsList[i].nom;
        selectListSoins.appendChild(option);
    }
    let selectPureComponentSoins = new vanillaSelectBox('#list-of-treatement', {
        "search": true,
        "keepInlineStyles": true,
        "minWidth": "100%",
        "placeHolder": "Select treatment"
    })
}

function showChart(data) {
    var barChartData = {
        labels: data.label,
        datasets: [
            {
                label: "Number of consultation",
                backgroundColor: ['rgba(0, 0, 255, 0.4)'],
                borderWidth: 1,
                data: data.data.map((d) => d.nbconsultation),
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30,
                yAxisID: 'right-y-axis'
            },
            {
                label: "Price",
                backgroundColor: ['rgba(255, 0, 0, 0.4)'],
                borderWidth: 1,
                data: data.data.map((d) => d.price),
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30,
                yAxisID: 'left-y-axis'
            }

        ]
    }
    var chartOptions = {
        responsive: true,
        
        legend: {
            display: 'false',
            position: 'bottom'
        },
        scales: {
            yAxes: [{
                id: 'left-y-axis',
                type: 'linear',
                position: 'left',
                ticks: {
                    beginAtZero: true,
                    precision: 0
                },
                 scaleLabel: {
                    display: true,
                    labelString: "Price"
                }
            }, {
                id: 'right-y-axis',
                type: 'linear',
                position: 'right',
                ticks:{
                    beginAtZero: true,
                    precision:0,
                },
                scaleLabel:{
                    display: true,
                    labelString: "Consultation"
                }
            }]
        }
    }
    myBar.clear();
    myBar.data.datasets = barChartData.datasets
    myBar.data.labels = barChartData.labels
    myBar.options = chartOptions
    myBar.update()
    el("#label-zone-data").text(customData.getLabel());
}

(function (){
    customData = new CustomData()

    var canvas = document.getElementById("canvas-consultation")
    var ctx = canvas.getContext("2d");
    window.myBar = new Chart(ctx, {
        title: {
            display: true
        },
        type: "bar",
    });

    ipcRenderer.invoke("list-consultation-event")
    .then((result)=>{
        console.log("Result ",result)
        customData.setData(result)
        showChart(customData.showData())
    })
    $(".left_btn").on("click",()=>{
        customData.previous()
        showChart(customData.showData())
    })
    $(".right_btn").on("click", () => {
        customData.next()
        showChart(customData.showData())
    })
    $("#week-consultation-visual").on("click", () => {
        console.log("week")
        customData.setTypeMesure(DateMessure.weekly)
        showChart(customData.showData())
    })
    $("#month-consultation-visual").on("click",()=>{
        customData.setTypeMesure(DateMessure.month)
        showChart(customData.showData())
    })
    $("#year-consultation-visual").on("click", () => {
        customData.setTypeMesure(DateMessure.year)
        showChart(customData.showData())
    })
    if (!soinsList) {
        ipcRenderer.invoke('list-soins-event')
            .then((result) => {
                soinsList = result                         
                setSelectSoinsComponents()
            })
    }

    $("#list-of-treatement").on("change", (e) => {
        let optionSelected = $("#list-of-treatement option:selected", this)
        customData.setTextFilter(optionSelected.toArray().map((option) => option.value))
        showChart(customData.showData())
    })


})()