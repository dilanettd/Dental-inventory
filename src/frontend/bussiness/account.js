let patientList = store.get("patients")
let selectedPatient = null;

function addPatient() {
    let patient = {
        firstname: el("#add-custom-fn").value,
        lastname: el("#add-custom-ln").value,
        datenaiss: el("#add-custom-db").value,
        placenaiss: el("#add-custom-bp").value,
        tel: el("#add-custom-tel").value,
        email: el("#add-custom-email").value,
        typePatient: el("#add-custom-top").value,
        sexe: el("#add-custom-sexe").value,
        adress: el("#add-custom-add").value,
    }

    if (
        patient.firstname.length == 0 ||
        patient.lastname.length == 0 ||
        patient.sexe.length == 0 ||
        patient.tel.length == 0 ||
        patient.typePatient.length == 0
    ) return notifier.alert("Invalid form")

    el('#add-customer-spinner').show(true)
    el("#submit-add-btn").disabled = true;
    ipcRenderer.invoke('add-customer-event', patient)
        .then((result) => {
            if (result.result.ok == true) {
                patientList.push(result.result.obj)

                setCustomerViewWithAction()
                el("#cancel-add-btn").click()
                setCustomerToModal()
                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el('#add-customer-spinner').show(false)
            el("#submit-add-btn").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el('#add-customer-spinner').show(false)
            el("#submit-add-btn").disabled = false;
            notifier.alert('Error occur')
        })
}


function setCustomerToModal(data) {
    if (data) selectedPatient = patientList.find((patient) => patient.matricule == data)

    else {
        selectedPatient = {
            nom: "",
            prenom: "",
            dateN: "",
            lieuN: "",
            tel: "",
            mail: "",
            typePatient: "",
            sexe: "",
            adresse: ""
        }
    }
    el("#edit-custom-fn").value = selectedPatient.nom
    el("#edit-custom-ln").value = selectedPatient.prenom

    el("#edit-custom-db").value = selectedPatient.dateN
    el("#edit-custom-bp").value = selectedPatient.lieuN
    el("#edit-custom-tel").value = selectedPatient.tel
    el("#edit-custom-email").value = selectedPatient.mail
    el("#edit-custom-top").value = selectedPatient.typePatient
    el("#edit-custom-sexe").value = selectedPatient.sexe
    el("#edit-custom-add").value = selectedPatient.adresse
}

function deletePatient(data) {
    // el(`[data-consultationid='${data}']`).click()
    notifier.confirm(
        'Do you realy want to delete this item?',
        () => {
            ipcRenderer.invoke('delete-customer-matricule', data)
                .then((result) => {
                    // console.log("End result ",result)
                    if (result.result == true) {
                        let pos = patientList.findIndex((patient) => patient.matricule == data)
                        if (pos >= 0) patientList.splice(pos, 1)
                        store.put("patients", patientList);
                        setCustomerViewWithAction()
                        notifier.success("Successful operation")

                    }
                })
        },
        {
            labels: {
                confirm: 'Attention'
            }
        }
    )
}

function updatePatient() {
    let pos = patientList.findIndex((s) => s.matricule == selectedPatient.matricule)

    let patient = {
        firstname: el("#edit-custom-fn").value,
        lastname: el("#edit-custom-ln").value,
        datenaiss: el("#edit-custom-db").value,
        placenaiss: el("#edit-custom-bp").value,
        tel: el("#edit-custom-tel").value,
        email: el("#edit-custom-email").value,
        typePatient: el("#edit-custom-top").value,
        sexe: el("#edit-custom-sexe").value,
        adress: el("#edit-custom-add").value,
        matricule: selectedPatient.matricule,
    }
    // console.log(patient)
    if (
        patient.firstname.length == 0 ||
        patient.lastname.length == 0 ||
        patient.sexe.length == 0 ||
        patient.tel.length == 0 ||
        patient.typePatient.length == 0
    ) return notifier.alert("Invalid form")

    selectedPatient.nom = patient.firstname;
    selectedPatient.prenom = patient.lastname
    selectedPatient.dateN = patient.datenaiss
    selectedPatient.lieuN = patient.placenaiss
    selectedPatient.tel = patient.tel
    selectedPatient.mail = patient.email
    selectedPatient.typePatient = patient.typePatient
    selectedPatient.sexe = patient.sexe
    selectedPatient.adresse = patient.adress


    el('#edit-customer-spinner').show(true)
    el("#submit-edit-btn").disabled = true;
    ipcRenderer.invoke('update-customer-event', patient)
        .then((result) => {
            console.log("End result ", result)
            if (result.result == true) {
                patientList[pos] = selectedPatient

                setCustomerViewWithAction()
                el("#cancel-edit-btn").click()
                setCustomerToModal()
                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el('#edit-customer-spinner').show(false)
            el("#submit-edit-btn").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el('#edit-customer-spinner').show(false)
            el("#submit-edit-btn").disabled = false;
            notifier.alert('Error occur')
        })
}

function setCustomerViewWithAction() {
    $("#table-customer").DataTable().destroy()
    // console.log("Consult ",consultationList)
    $("#table-customer").DataTable({
        data: patientList,
        columns: [
            { data: 'matricule' },
            { data: 'nom' },
            { data: 'prenom' },
            { data: 'tel' },
            { data: "typePatient" },
            {
                data: 'matricule',
                render: (data) => {
                    return `<td align="center">
                        <a data-bs-toggle="modal" role="button" href="#editCustomer" onclick="setCustomerToModal('${data}')"
                                class="btn btn-outline-secondary fas fa-edit"> </a>
                        <a href="#"
                            onclick="deletePatient('${data}')"
                            class="btn btn-outline-danger fas fa-trash"> </a>
                        </td>`
                }
            }
        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    })
}

(function () {
    if (!patientList) {
        ipcRenderer.invoke("list-patient-event")
            .then((result) => {
                console.log("patient-liste",new Date())
                patientList = result
                setCustomerViewWithAction()
            })
    }

})()