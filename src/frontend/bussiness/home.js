
let consultationList = store.get("consultations")
let actionList = store.get("actions")

$("#consultation-table").DataTable({})

function showCountToDayProductReleaseList() {
    // document.querySelectorAll("[data-show='today-count-release-product']")
    // .forEach((e)=>e.textContent=historiqueServiceInstance.getNumberObjectByDay(`${new Date().getFullYear()}`,`${new Date().getMonth()}`,`${new Date().getDay()}`))
}

function showCountToDayConsultation() {
    console.log("showConte")
    // document.querySelectorAll("[data-show='today-count-consultation']")
    // .forEach((e)=>e.textContent=consultationServiceInstance.getNumberObjectByDay(`${new Date().getFullYear()}`,`${new Date().getMonth()}`,`${new Date().getDay()}`))
}

function setConsultationView() {
    $("#consultation-table").DataTable().destroy()
    console.log("Consult ", consultationList)
    let dataTable = $("#consultation-table").DataTable({
        "order": [[3, "asc"]],
        data: consultationList.map((data, index) => {
            return {
                "id":data.id,
                "customer_name": `${data.patient.nom} ${data.patient.prenom}`,
                "type_of_customer": `${data.patient.typePatient}`,
                "date_of_consultation": `${data.jour}/${data.moi}/${data.annee}`,
                "prix": data.prix,
            }
        }),
        columns: [
            { data: 'id' },
            { data: 'customer_name' },
            { data: 'type_of_customer' },
            { data: 'date_of_consultation' },
            { data: "prix" }
        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    })
    dataTable.on('order.dt search.dt', function () {
        dataTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function setActionsView() {
    $("#actions-table").DataTable().destroy()
    console.log("Actions ", actionList)
    let table=$("#actions-table").DataTable({
        "order": [[4, "asc"]],
        data: actionList.map((data, index) => {
            return {
                "id": data.id,
                "name": `${data.produit.nom}`,
                "trad_name": `${data.produit.nomCommercial}`,
                "expire_date": `${data.jour}/${data.moi}/${data.annee}`,
                "quantite": data.quantite,
                "packaging":data.produit.conditionnement
            }
        }),
        columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'trad_name' },
            { data: 'quantite' },
            { data: "expire_date" },
            { data: "packaging" }

        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    })
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}



(function () {
    ipcRenderer.invoke('count-todayconsultation-event', { day: `${new Date().getDate()}`, month: `${new Date().getMonth() + 1}`, year: `${new Date().getFullYear()}` })
        .then((result) => el("[data-show='today-count-consultation']").text(result))

    ipcRenderer.invoke('count-today-product-release-event', { day: `${new Date().getDate()}`, month: `${new Date().getMonth() + 1}`, year: `${new Date().getFullYear()}` })
        .then((result) => el("[data-show='today-count-release-product']").text(result))

    if (!consultationList) {
        ipcRenderer.invoke('list-today-consultation-event', { day: `${new Date().getDate()}`, month: `${new Date().getMonth()+1}`, year: `${new Date().getFullYear()}` })
            .then((result) => {
                consultationList = result
                store.put("consultations", result)
                setConsultationView("#consultation-table")
            })
    }
    if (!actionList) {
        ipcRenderer.invoke('list-today-product-release-event',{ day: `${new Date().getDate()}`, month: `${new Date().getMonth() +1}`, year: `${new Date().getFullYear()}` })
            .then((result) => {
                actionList = result
                setActionsView()
            })
    }
})()


