
let consultationList = store.get("consultations")
let soinsList = store.get("soins")
let patientList = store.get("patients")
let selectedSoins = null;


// console.log(selectPureComponentSoins)

function addTreatement() {
    let treatement = {
        name: el("#name-treatement").value,
        description: el("#description-treatement").value,
        standart_price: el("#standard-prix-treatment").value,
        staff_price: el("#staff-prix-treatment").value,
        insured_price: el("#insured-prix-treatment").value
    }

    if (treatement.name.length == 0 ||
        treatement.description.length == 0 ||
        treatement.standart_price == 0 ||
        treatement.staff_price == 0 ||
        treatement.insured_price == 0
    ) {
        return notifier.alert("Invalid form")
    }
    el('#submit-spinner-treatment').show(true)
    el("#btn-add-treatment").disabled = true;
    ipcRenderer.invoke('add-soins-event', treatement)
        .then((result) => {
            // console.log("End result ",result)
            if (result.result.ok == true) {
                soinsList.push(result.result.obj)
                store.put("soins", soinsList)
                el("#name-treatement").value = ""
                el('#description-treatement').value = ""
                el("#standard-prix-treatment").value = ""
                el("#staff-prix-treatment").value = ""
                el("#insured-prix-treatment").value = ""
                setTreatementViewWithAction()
                setSelectSoinsComponents()
                el("#cancel-btn-treatment").click()

                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el('#submit-spinner-treatment').show(false)
            el("#btn-add-treatment").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el('#submit-spinner-treatment').show(false)
            el("#btn-add-treatment").disabled = false;
            notifier.alert('Error occur')
        })

}


function addConsultation() {
    let consult = {
        customer_name: el('#customer_full_name').value,
        date_consultation: el("#date_consultation").value,
        soins: $("#list-of-treatement").val(),
        prix: parseInt(el("#value-price-total").text()),
        comment: el("#treatement_comment").value
    }

    if (consult.customer_name.length == 0 || consult.date_consultation.length == 0 || consult.soins.length == 0) {
        return notifier.alert("Invalid form")
    }
    el('#add-submit-spinner').show(true)
    el("#btn-add-consultation").disabled = true;
    let p = patientList.find((patient) => `${patient.nom} ${patient.prenom}` == consult.customer_name)
    consult["matricule_customer"] = p.matricule
    console.log("patient ", p)
    //  notifier.confirm(
    // 'Do you realy want to add this item?',
    ipcRenderer.invoke('add-consultation-event', consult)
        .then((result) => {
            console.log("End result ", result)
            if (result.result.ok == true) {
                consultationList.push(result.result.obj)
                store.put("consultations", consultationList);
                el("#btn-cancel-add-consultation").click()
                el('#customer_full_name').value = ""
                el("#date_consultation").value = ""

                el("#list-of-treatement").selectedIndex = 0
                el("#treatement_comment").value = ""
                setConsultationViewWithAction()
                notifier.success("Successful operation")
            }
            else {
                notifier.alert('Error occur')
            }
            el('#add-submit-spinner').show(false)
            el("#btn-add-consultation").disabled = false;
        })
        .catch((error) => {
            console.log("Error ", error)
            el('#add-submit-spinner').show(false)
            el("#btn-add-consultation").disabled = false;
            notifier.alert('Error occur')
        })
}

function deleteConsultation(data) {
    // el(`[data-consultationid='${data}']`).click()
    notifier.confirm(
        'Do you realy want to delete this item?',
        () => {
            ipcRenderer.invoke('delete-consultation-by-id', data)
                .then((result) => {
                    // console.log("End result ",result)
                    if (result.result == true) {
                        let pos = consultationList.findIndex((consultation) => consultation.id == data)
                        if (pos >= 0) consultationList.splice(pos, 1)
                        store.put("consultations", consultationList);
                        setConsultationViewWithAction()
                        notifier.success("Successful operation")

                    }
                })
        },
        {
            labels: {
                confirm: 'Attention'
            }
        }
    )
}

function saveSoins() {
    let pos = soinsList.findIndex((s) => s.id == selectedSoins.id)
    el(".modal-treatment [data-display='false']").show(true)
    el('.modal-treatment #text-submit').show(false)
    el('.modal-treatment .submit-update-treatement').disabled = true

    selectedSoins.nom = el("[data-show='treatment-name']").value
    selectedSoins.description = el("[data-show='treatment-description']").value
    selectedSoins.prixContoire = el("[data-show='treatment-standard-price']").value
    selectedSoins.prixPersonnel = el("[data-show='treatment-staff-price']").value
    selectedSoins.prixAssurer = el("[data-show='treatment-insured-price']").value
    ipcRenderer.invoke('update-soins-item-event', selectedSoins)
        .then((result) => {
            if (result.result) {
                soinsList[pos] = selectedSoins;
                setDataToModal()
                el('[data-event="close-treatment-modal"]').click()
                notifier.success("Success of the update")
                setTreatementViewWithAction()
                setSelectSoinsComponents()
                selectedSoins = null;
                store.put("soins", soinsList);

            }
            else {
                notifier.alert("An error occured")
            }
            el(".modal-treatment [data-display]").show(false)
            el('.modal-treatment #text-submit').show(true)
            el('.modal-treatment .submit-update-treatement').disabled = false
        })
        .catch((error) => {
            console.log("error ", error)
            notifier.alert("An error occured")
            el(".modal-treatment [data-display='false']").show(false)
            el('.modal-treatment #text-submit').show(true)
            el('.modal-treatment .submit-update-treatement').disabled = false
        })
}

function deleteSoins(data) {
    notifier.confirm(
        'Do you realy want to delete this item ?',
        () => {
            ipcRenderer.invoke('delete-soins-by-id', data)
                .then((result) => {
                    if (result.result == true) {
                        notifier.success("Successful operation")
                        let pos = soinsList.findIndex((soins) => soins.id == data)
                        if (pos >= 0) soinsList.splice(pos, 1)
                        store.put("soins", soinsList);
                        setTreatementViewWithAction()
                        setSelectSoinsComponents()
                    }
                })
        },
        {
            labels: {
                confirm: 'Attention'
            }
        }
    )
}

function showConsultation(id) {
    let consultation = consultationList.find((consult) => consult.id == id)
    if (!consultation) return;
    console.log("Consultation ", consultation, `${parseInt(consultation.jour) < 10 ? "0" + consultation.jour : consultation.jour}/${parseInt(consultation.mois) < 10 ? "0" + consultation.mois : consultation.mois}/${consultation.annee}`)
    el("[data-show='consultation-show-name']").value = `${consultation.patient.nom} ${consultation.patient.prenom}`
    el("[data-show='consultation-show-date']").value = `${parseInt(consultation.jour) < 10 ? "0" + consultation.jour : consultation.jour}/${parseInt(consultation.mois) < 10 ? "0" + consultation.mois : consultation.mois}/${consultation.annee}`
    el("[data-show='consultation-show-number']").text(`(${consultation.soins.length})`)
    el("[data-show='consultation-show-list']").innerHTML = "";
    consultation.soins.forEach((soins) => {
        let li = document.createElement('li')
        li.textContent = soins.nom;
        el("[data-show='consultation-show-list']").appendChild(li)
    })

    el("[data-show='consultation-show-total']").text(consultation.prix)
    el("[data-show='consultation-show-comment']").value = consultation.commentaire

}

function setConsultationViewWithAction() {
    $("#consultation-table").DataTable().destroy()
    // console.log("Consult ",consultationList)
    let table =$("#consultation-table").DataTable({
        "order": [[3, "asc"]],
        data: consultationList.map((data, index) => {
            return {
                "id": "",
                "customer_name": `${data.patient.nom} ${data.patient.prenom}`,
                "type_of_customer": `${data.patient.typePatient}`,
                "date_of_consultation": `${data.jour}/${data.moi}/${data.annee}`,
                "prix": data.prix,
                "consultation_id": data.id
            }
        }),//<a data-bs-toggle="modal" role="button" href="#editConsulstation" class="btn btn-outline-secondary fas fa-edit"></a>
        columns: [
            { data: 'id' },
            { data: 'customer_name' },
            { data: 'type_of_customer' },
            { data: 'date_of_consultation' },
            { data: "prix" },
            {
                data: 'consultation_id',
                render: (data) => {
                    return `<td align="center">
                                <a data-bs-toggle="modal" role="button" href="#showConsultation" data-consultationid="${data}" onclick="showConsultation(${data})" class="btn btn-outline-secondary fas fa-eye"></a>
                                <a href="#" data-consultationid="${data}" onclick="deleteConsultation(${data})" class="btn btn-outline-danger fas fa-trash"></a>
                            </td>`
                }
            }
        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    })
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function setSelectSoinsComponents() {
    let selectListSoins = document.querySelector("#list-of-treatement")
    selectListSoins.innerHTML = "";
    for (var i = 0; i < soinsList.length; i++) {
        var option = document.createElement("option");
        option.value = soinsList[i].id;
        option.text = soinsList[i].nom;
        selectListSoins.appendChild(option);
    }
    let selectPureComponentSoins = new vanillaSelectBox('#list-of-treatement', {
        "search": true,
        "keepInlineStyles": true,
        "minWidth": "100%"
    })
}
function setTreatementViewWithAction() {
    $("#soins-table").DataTable().destroy()
    // console.log("Consult ",consultationList)
    let table=$("#soins-table").DataTable({
        data: soinsList,//
        columns: [
            { data: 'id' },
            { data: 'nom' },
            { data: 'description' },
            { data: 'prixContoire' },
            { data: 'prixPersonnel' },
            { data: "prixAssurer" },
            {
                data: 'id',
                render: (data) => {
                    return `<td align="center">
                                <a data-bs-toggle="modal" role="button" href="#editTreatment" onclick="setDataToModal(${data})" class="btn btn-outline-secondary fas fa-edit"></a>
                                <a href="#" data-consultationid="${data}"  onclick="deleteSoins(${data})" class="btn btn-outline-danger fas fa-trash"></a>
                            </td>`
                }
            }
        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    })
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function setDataToModal(id) {
    let soins = soinsList.find((s) => s.id == id)
    if (!soins) {
        el("[data-show='treatment-name']").value = ""
        el("[data-show='treatment-description']").value = ""

        el("[data-show='treatment-standard-price']").value = 0

        el("[data-show='treatment-staff-price']").value = 0

        el("[data-show='treatment-insured-price']").value = 0
        return;
    };
    el("[data-show='treatment-name']").value = soins.nom
    el("[data-show='treatment-description']").value = soins.description

    el("[data-show='treatment-standard-price']").value = soins.prixContoire

    el("[data-show='treatment-staff-price']").value = soins.prixPersonnel

    el("[data-show='treatment-insured-price']").value = soins.prixAssurer
    selectedSoins = soins
}

function setPrixWhenSelectedItemInAddConsultation() {
    el("#value-price-total").text("0")
    let optionSelected = $("#list-of-treatement option:selected", this)
    el("#value-price-total")
        .text(
            optionSelected.toArray().map((option) => {
                let s = soinsList.find((soins) => soins.id == `${option.value}`)
                if (s && $("#customer_full_name").val().length > 0) {
                    let p = patientList.find((patient) => `${patient.nom} ${patient.prenom}` == $("#customer_full_name").val())
                    console.log("p ", p)
                    if (p) {
                        if (p.typePatient == "personnel") return parseInt(s.prixPersonnel)
                        if (p.typePaient == "assurer") return parseInt(s.prixAssurer)
                        else return parseInt(s.prixContoire)
                    }
                }
                return 0
            }).reduce((prev, curr) => curr + prev, 0)
        )
}

(function () {
    if (!consultationList) {
        ipcRenderer.invoke('list-consultation-event')
            .then((result) => {
                consultationList = result
                console.log("Consultation",result)
                store.put("consultations", result)
                setConsultationViewWithAction("#consultation-table")
            })
    }

    if (!soinsList) {
        ipcRenderer.invoke('list-soins-event')
            .then((result) => {
                soinsList = result
                store.put("soins", result)
                // selectPureComponentSoins.enableItems(soinsList.map((e)=>e.nom))
                //list-of-treatement           
                setSelectSoinsComponents()
                setTreatementViewWithAction()

            })
    }

    if (!patientList) {
        ipcRenderer.invoke("list-patient-event")
            .then((result) => {
                patientList = result
                console.log("result", patientList)
                $("#customer_full_name").autocomplete({
                    source: patientList.map((patient) => `${patient.nom} ${patient.prenom}`),
                    close: (event, ui) => setPrixWhenSelectedItemInAddConsultation()
                })
            })
    }
    $("#customer_full_name").on("blur", (event) => {
        setPrixWhenSelectedItemInAddConsultation()
    })

    $("#list-of-treatement").on("change", (e) => {
        setPrixWhenSelectedItemInAddConsultation()
    })
})()