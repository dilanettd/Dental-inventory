let itemList = store.get("items")
let selectedItem = {}
let action = ""

function get_setValue(modalid, dataid) {

    if (dataid) selectedItem = itemList.find((item) => item.id == dataid)
    else {
        selectedItem = {
            id: "",
            nom: "",
            nomCommercial: "",
            imageUrl: "",
            quantite: 0,
            datePeremption: "",
            fabricant: "",
            conditionnement: "",
        }
    }

    el(modalid + " #item-name").value = selectedItem.nom
    el(modalid + " #item-trade-name").value = selectedItem.nomCommercial

    el(modalid + " #item-qte").value = selectedItem.quantite
    el(modalid + " #item-exp-date").value = selectedItem.datePeremption
    el(modalid + " #item-packaging").value = selectedItem.conditionnement
    el(modalid + " #item-manufacturer").value = selectedItem.fabricant
}

function getAll(modalid) {
    selectedItem.nom = el(modalid + " #item-name").value
    selectedItem.nomCommercial = el(modalid + " #item-trade-name").value
    selectedItem.quantite = el(modalid + " #item-qte").value
    selectedItem.fabricant = el(modalid + " #item-packaging").value
    selectedItem.conditionnement = el(modalid + " #item-manufacturer").value

    return {
        nom: el(modalid + " #item-name").value,
        nomCommercial: el(modalid + " #item-trade-name").value,
        id: selectedItem.id,
        quantite: el(modalid + " #item-qte").value,
        datePeremption: el(modalid + " #item-exp-date").value,
        fabricant: el(modalid + " #item-packaging").value,
        conditionnement: el(modalid + " #item-manufacturer").value
    }
}

function addItem(modalid) {
    let item = getAll(modalid)
    if (
        item.nom.length == 0 ||
        item.nomCommercial.length == 0
    ) return notifier.alert("Invalid form")

    el(modalid + ' #submit-spinner').show(true)
    el(modalid + " #btn-save").disabled = true;
    ipcRenderer.invoke('add-item-event', item)
        .then((result) => {
            console.log("result ", result)
            if (result.result.ok == true) {
                itemList.push(result.result.obj)
                setItemViewWithAction()
                el(modalid + " #btn-cancel").click()
                get_setValue(modalid)
                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el(modalid + ' #submit-spinner').show(false)
            el(modalid + " #btn-save").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el(modalid + ' #submit-spinner').show(false)
            el(modalid + " #btn-save").disabled = false;
            notifier.alert('Error occur')
        })
}

function editItem(modalid) {
    let item = getAll(modalid)
    if (
        item.nom.length == 0 ||
        item.nomCommercial.length == 0
    ) return notifier.alert("Invalid form")

    el(modalid + ' #submit-spinner').show(true)
    el(modalid + " #btn-save").disabled = true;
    ipcRenderer.invoke('update-item-event', item)
        .then((result) => {
            if (result.result == true) {
                let pos = itemList.findIndex((item) => item.id == selectedItem.id)
                itemList[pos] = selectedItem
                selectedItem = {};
                setItemViewWithAction()
                el(modalid + " #btn-cancel").click()
                get_setValue(modalid)
                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el(modalid + ' #submit-spinner').show(false)
            el(modalid + " #btn-save").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el(modalid + ' #submit-spinner').show(false)
            el(modalid + " #btn-save").disabled = false;
            notifier.alert('Error occur')
        })
}

function setSelectedValue(id, ac) {
    selectedItem = itemList.find((item) => item.id == id)
    action = ac
    if (action == 'increase') el("[data-show='add-to-stock']").show(true)
    else el("[data-show='remove-to-stock']").show(true)
}

function cancelQuantity() {
    el("[data-show='add-to-stock']").show(false)
    el("[data-show='remove-to-stock']").show(false)
}

function saveQuantity() {
    let value = parseInt(el("#item-quantity-action").value)
    if (action == 'increase' && value <= 0) {
        cancelQuantity()
        return notifier.alert("Enter a positive value")
    }
    else if (action == 'decrease' && value > selectedItem.quantite) {
        cancelQuantity()
        return notifier.alert("The entered value is greater than the stock")
    }
    el('#quantity-submit-spinner').show(true)
    el("#quantity-btn-save").disabled = true;
    ipcRenderer.invoke('update-item-quantiti', { id: selectedItem.id, quantite: value, action: action == 'increase' ? 'add_action' : 'delete_action' })
        .then((result) => {
            if (result.result == true) {
                let pos = itemList.findIndex((item) => item.id == selectedItem.id)
                if (action == 'increase') itemList[pos].quantite += value
                else itemList[pos].quantite -= value
                selectedItem = {};
                setItemViewWithAction()
                el("#item-quantity-action").value=0;
                el("#quantity-btn-cancel").click()
                notifier.success("Successful operation")
            }
            else notifier.alert('Error occur')
            el('#quantity-submit-spinner').show(false)
            el("#quantity-btn-save").disabled = false;
        }).catch((error) => {
            console.log("Erreur ", error)
            el('#quantity-submit-spinner').show(false)
            el("#quantity-btn-save").disabled = false;
            notifier.alert('Error occur')
        })
}

function setItemViewWithAction() {
    $("#table-item").DataTable().destroy()
    // console.log("Consult ",consultationList)
    let dataTable = $("#table-item").DataTable({
        data: itemList,
        columns: [
            { data: 'id' },
            { data: 'nom' },
            { data: 'nomCommercial' },
            { data: 'quantite' },
            {
                data: 'id',
                render: (data) => {
                    return `<td align="center">
                        <a data-bs-toggle="modal" role="button" href="#showItems" onclick="get_setValue('#showItems','${data}')"
                                class="btn btn-outline-secondary fas fa-eye"> </a>
                        <a data-bs-toggle="modal" role="button" href="#editItems" onclick="get_setValue('#editItems','${data}')"
                                class="btn btn-outline-secondary fas fa-edit"> </a>
                        <a href="#quantityItems" data-bs-toggle="modal" role="button"
                            onclick="setSelectedValue('${data}','increase')"
                            class="btn btn-outline-primary fas fa-plus"> </a>
                        <a href="#quantityItems" data-bs-toggle="modal" role="button"
                            onclick="setSelectedValue('${data}','decrease')"
                            class="btn btn-outline-danger fas fa-minus"> </a>
                        </td>`
                }
            }
        ],
        "iDisplayLength": 10,
        "bInfo": false,
        "lengthChange": false,

        language: {
            emptyTable: "No data available",
            search: '',
            searchPlaceholder: "Search...",
            paginate: {
                previous: '<i class="fas fa-angle-left fa-2x"></i>',
                next: '<i class="fas fa-angle-right fa-2x"></i>'
            }
        }
    });
    dataTable.on('order.dt search.dt', function () {
        dataTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

(function () {
    if (!itemList) {
        ipcRenderer.invoke("list-items-event")
            .then((result) => {
                console.log("Produit ", result)
                itemList = result
                setItemViewWithAction()
            })
    }

})()