let stockData = null
let productList = store.get("soins")

class StockData {
    constructor() {
        this.date = new Date();
        this.currentDateValue = this.getDailyDate(new Date());
        this.typemessure = DateMessure.weekly;
        this.data = []
        this.selectedFilter = []
        this.currentData = [];
    }

    setData(data) {

        this.data = data.map((d) => {
            // console.log("D",d)
            d["date"] = `${parseInt(d.jour) < 10 ? "0" + d.jour : d.jour}/${parseInt(d.moi) < 10 ? "0" + d.moi : d.moi}/${d.annee}`
            return d
        }
        );
    }
    setTypeMesure(type) {
        this.typemessure = type
    }
    setTextFilter(filter) {
        this.selectedFilter = filter
    }

    previous() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() - 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() - 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() - 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() - 1)
                break
        }
    }

    next() {
        switch (this.typemessure) {
            case DateMessure.day:
                this.date.setDate(this.date.getDate() + 1);
                break;
            case DateMessure.weekly:
                this.date.setDate(this.date.getDate() + 6);
                break
            case DateMessure.month:
                this.date.setMonth(this.date.getMonth() + 1);
                break
            case DateMessure.year:
                this.date.setFullYear(this.date.getFullYear() + 1)
                break
        }
    }

    getLabels() {
        let labels = []
        switch (this.typemessure) {
            case DateMessure.day:
                return this.currentData.map((data) => data.date);
            case DateMessure.weekly:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(dayOfWeek[(new Date(parseInt(date.split("/")[2]), parseInt(date.split("/")[1]) - 1, parseInt(date.split("/")[0]))).getDay()])
                });
                return labels
            case DateMessure.month:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(`${date.split("_")[0].split("/")[0]} - ${date.split("_")[1].split("/")[0]}`)
                });
                return labels
            case DateMessure.year:
                Array.from(this.currentData.keys()).forEach((date) => {
                    if (this.currentData.get(date).length != 0) labels.push(monthOfYear[date])
                });
                return labels
        }



        // switch (this.typemessure) {
        //     case DateMessure.day:
        //         return this.currentData.map((data) => data.date);
        //     case DateMessure.weekly:
        //         return Array.from(this.currentData.keys()).map((date) => dayOfWeek[(new Date(parseInt(date.split("/")[2]), parseInt(date.split("/")[1]) - 1, parseInt(date.split("/")[0]))).getDay()]);
        //     case DateMessure.month:
        //         return Array.from(this.currentData.keys()).map((date) => `${date.split("_")[0].split("/")[0]} - ${date.split("_")[1].split("/")[0]}`);
        //     case DateMessure.year:
        //         return Array.from(this.currentData.keys()).map((data) => monthOfYear[data])
        // }
    }
    textFilter() {
        if (this.selectedFilter.length == 0) return;
        Array.from(this.currentData.keys()).forEach((key) => {
            this.currentData.set(key, this.currentData.get(key).filter((product) => {
                let exist = false;
                console.log("product",product,this.selectedFilter)
                this.selectedFilter.forEach((filter) => {
                    if (product.typeAction==filter) exist = true
                })
                return exist
            }))
        })
    }
    applyFilter() {
        let getMeasureTable = [];
        switch (this.typemessure) {
            case DateMessure.day:
                this.currentData = this.data.filter((data) => {
                    return data.date == this.getDailyDate(this.date)
                });
                this.textFilter()
                return this.currentData;
            case DateMessure.weekly:
                this.currentData = new Map();
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                this.data.forEach((data) => {
                    let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                    if (currDate.getFullYear() == this.date.getFullYear() && currDate.getMonth() == this.date.getMonth()) {
                        if (firstDayOfWeek <= currDate && lastDayOfWeek >= currDate) {
                            if (this.currentData.has(data.date)) this.currentData.get(data.date).push(data);
                            else this.currentData.set(data.date, [data])
                        }
                    }

                })
                getMeasureTable = []
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
            case DateMessure.month:
                getMeasureTable = []
                let firstDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
                let lastDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
                this.currentData = new Map();
                let i = firstDayOfMonth.getDate();
                while (i <= lastDayOfMonth.getDate()) {
                    this.data.forEach((data) => {
                        let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                        if (currDate.getFullYear() == firstDayOfMonth.getFullYear() && currDate.getMonth() == firstDayOfMonth.getMonth()) {
                            if (new Date(currDate.getFullYear(), currDate.getMonth(), i) <= currDate && new Date(currDate.getFullYear(), currDate.getMonth(), i + 6) >= currDate) {
                                let k = new Date(currDate.getFullYear(), currDate.getMonth(), i);
                                let k1 = new Date(currDate.getFullYear(), currDate.getMonth(), i + 6);
                                if (this.currentData.has(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`)) this.currentData.get(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`).push(data);
                                else this.currentData.set(`${this.getDailyDate(k)}_${this.getDailyDate(k1)}`, [data])
                            }
                        }
                    })
                    i += 7;
                }
                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
            case DateMessure.year:
                getMeasureTable = [];
                this.currentData = new Map();
                this.data.forEach((data) => {
                    let currDate = new Date(parseInt(data.date.split("/")[2]), parseInt(data.date.split("/")[1]) - 1, parseInt(data.date.split("/")[0]));
                    if (currDate.getFullYear() == this.date.getFullYear()) {
                        if (this.currentData.has(currDate.getMonth())) this.currentData.get(currDate.getMonth()).push(data)
                        else this.currentData.set(currDate.getMonth(), [data])
                    }
                })

                for (let values of this.currentData.values()) {
                    getMeasureTable = getMeasureTable.concat(values);
                }
                this.textFilter()
                return getMeasureTable;
        }
    }

    generateData() {
        let tabData = []
        switch (this.typemessure) {
            case DateMessure.month:
            case DateMessure.year:
            case DateMessure.weekly:
                tabData = [];
                console.log("value", this.currentData)
                Array.from(this.currentData.keys()).forEach((key) => {
                    let itemResult = this.currentData.get(key).map((product) => product.quantite).reduce((acc, curr) => parseInt(curr) + acc, 0)

                    tabData.push({
                        "nbaction": this.currentData.get(key).length,
                        "quantity": itemResult
                    })
                })
                // if (this.typemessure == DateMessure.weekly) tabData.sort((dataA, dataB) => new Date(dataA.key) <= new Date(dataB.key) ? -1 : +1)
                // if (this.typemessure == DateMessure.month) tabData.sort((dataA, dataB) => new Date(dataA.key.split("_")[0]) <= new Date(dataB.key.split("_")[0]) ? -1 : +1)

                //tabData.forEach((data)=> delete data.key);
                break

        }
        // this.currentData = tabData.slice();
        return {
            "label": this.getLabels(),
            "data": [...tabData]
        }
    }

    showData() {
        this.applyFilter()
        return this.generateData()
    }

    getDailyDate(d) {
        // let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        // return `${date.getFullYear()}-${date.getMonth() + 1}-${day}`
        return `${parseInt(d.getDate()) < 10 ? "0" + d.getDate() : d.getDate()}/${parseInt(d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1}/${d.getFullYear()}`
    }

    getLabel() {
        switch (this.typemessure) {
            case DateMessure.day:
                return this.date.getDate() + '-' + monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear();
            case DateMessure.weekly:
                let firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay());
                let lastDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() - this.date.getDay() + 6);
                // return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${firstDayOfWeek.getMonth()}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + lastDayOfWeek.getDate() : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth()}/${firstDayOfWeek.getFullYear()}`
                return `${firstDayOfWeek.getDate() < 10 ? "0" + firstDayOfWeek.getDate() : firstDayOfWeek.getDate()}/${(firstDayOfWeek.getMonth() + 1) < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : (firstDayOfWeek.getMonth() + 1)}/${firstDayOfWeek.getFullYear()} -- ${lastDayOfWeek.getDate() < 10 ? "0" + lastDayOfWeek.getDate() : lastDayOfWeek.getDate()}/${firstDayOfWeek.getMonth() + 1 < 10 ? "0" + (firstDayOfWeek.getMonth() + 1) : firstDayOfWeek.getMonth() + 1}/${firstDayOfWeek.getFullYear()}`

            case DateMessure.month:
                return monthOfYear[this.date.getMonth()] + '-' + this.date.getFullYear()
            case DateMessure.year:
                return this.date.getFullYear()
        }
    }
}


function showChartAction(data) {
    console.log("action ",data)
    let barChartData = {
        labels: data.label,
        datasets: [
            {
                label: "Product:",
                backgroundColor: ['rgba(0, 0, 255, 0.4)'],
                borderWidth: 1,
                data: data.data.map((d) => d.nbaction),
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30,
                yAxisID: 'right-y-axis'
            },
            {
                label: "Quantity",
                backgroundColor: ['rgba(255, 0, 0, 0.4)'],
                borderWidth: 1,
                data: data.data.map((d) => d.quantity),
                barThickness: 25,
                maxBarThickness: 30,
                minBarLength: 30,
                yAxisID: 'left-y-axis'
            }

        ]
    }
    let chartOptions = {
        responsive: true,
        legend: {
            display: 'false',
            position: 'bottom'
        },
        scales: {
            yAxes: [{
                id: 'left-y-axis',
                type: 'linear',
                position: 'left',
                ticks: {
                    beginAtZero: true,
                    precision: 0
                },
                scaleLabel: {
                    display: true,
                    labelString: "Price"
                }
            }, {
                id: 'right-y-axis',
                type: 'linear',
                position: 'right',
                ticks: {
                    beginAtZero: true,
                    precision: 0,
                },
                scaleLabel: {
                    display: true,
                    labelString: "Product"
                }
            }]
        }
    }
    myBarAction.clear();
    myBarAction.data.datasets = barChartData.datasets
    myBarAction.data.labels = barChartData.labels
    myBarAction.options = chartOptions
    myBarAction.update()
    el("#label-zone-data-action").text(stockData.getLabel());
}

(function () {
    stockData = new StockData()

    var canvas2 = document.querySelector("#canvas_action")
    var ctx2 = canvas2.getContext("2d");
    window.myBarAction = new Chart(ctx2, {
        type: "bar",
    });

    ipcRenderer.invoke("list-product-event")
        .then((result) => {
            console.log("Result ", result)
            stockData.setData(result)
        showChartAction(stockData.showData())
        })
    $(".left_btn-action").on("click", () => {
        stockData.previous()
        showChartAction(stockData.showData())
    })
    $(".right_btn-action").on("click", () => {
        stockData.next()
        showChartAction(stockData.showData())
    })
    $("#week-action-visual").on("click", () => {
        console.log("week")
        stockData.setTypeMesure(DateMessure.weekly)
        showChartAction(stockData.showData())
    })
    $("#month-action-visual").on("click", () => {
        stockData.setTypeMesure(DateMessure.month)
        showChartAction(stockData.showData())
    })
    $("#year-action-visual").on("click", () => {
        stockData.setTypeMesure(DateMessure.year)
        showChartAction(stockData.showData())
    })
    
    let selectPureComponentSoins2 = new vanillaSelectBox('#list-of-action', {
        "search": true,
        "keepInlineStyles": true,
        "minWidth": "100%",
        "placeHolder": "Select action"
    })
    $("#list-of-action").on("change", (e) => {
        let optionSelected = $("#list-of-action option:selected", this)
        stockData.setTextFilter(optionSelected.toArray().map((option) => option.value))
        showChartAction(stockData.showData())
    })


})()