/* importation de module*/
const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path')


/**
 * Initialisation du template
 */


/* initialisation de la fénétre*/

let mainWindow;
function createWindow() {
  mainWindow = new BrowserWindow(
    { 
      width: 1800, 
      height: 1200,
      webPreferences: {
        preload:path.join(__dirname, 'utils','preload.js'),
        nodeIntegration: false, // default in Electron >= 5
        contextIsolation: true, // default in Electron >= 12
      }
    }); // on définit une taille pour notre fenêtre

  mainWindow.loadURL("file://" + __dirname +'/frontend/pages/login.html'); // on doit charger un chemin absolu
//  mainWindow.loadURL('data:text/html;charset=utf-8,' + encodeURI(nunjucks.render("pages/login.njk")));
  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}
/* Appele de la methode createWindow lorsque l'applicatioon demarre*/
app.on("ready", createWindow);

/* On va devoir utiliser l’évènement window-all-closed d’app, qui est émit quand toutes les fenêtres ont étés fermées.
 À ce moment, on va utiliser la méthode app.quit qui va nous permettre de quitter l’application*/
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
const mainbackend = require("./backend/app.ts")